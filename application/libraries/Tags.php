<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tags {

    public function __construct()
    {
        $CI =& get_instance();
        $CI->load->model('tags_model');
        $CI->load->model('gallery_model');
        $CI->load->model('video_model');
        $CI->load->library('parser');
    }

    public function getTags($id, $tag_key) {
        $CI =& get_instance();
        return $CI->tags_model->getTags($id, $tag_key);
    }

    public function getTagged($tag_key, $tagged_objs) {
        $CI =& get_instance();
        $tagged_items = array();
        $item_tags = $CI->tags_model->getTagged($tag_key, $tagged_objs);
        if(!empty($item_tags))
            array_unshift($tagged_items, $item_tags);
        if(is_array($tagged_items) && !empty($tagged_items)) {
            switch($tag_key) {
                case 'gallery':
                    return $this->GetTaggedGallery($tagged_items);
                    break;
                case 'video':
                    return $this->GetTaggedVideo($tagged_items);
                    break;
                default:
                    return $tagged_items;
            }
        }
    }

    public function GetTaggedGallery($tagged_items) {
        $CI =& get_instance();
       // if(is_array($tagged_items) && !empty($tagged_items)) {
            $galleries = array();
        
            foreach($tagged_items[0] as $gallery) {
                $galleries = array_merge_recursive($galleries, $CI->gallery_model->get_galleries_photos($gallery['obj_id'], 0, 1000));
            }

            $result['gallery_images'] = $galleries;
            //$page_content['tagged_gallery'] =
            $gallery = $CI->parser->parse('_partial_gallery',$result,TRUE);
            return $gallery;
        //}
    }

    public function GetTaggedVideo($tagged_items) {
        $CI =& get_instance();
        $videos = array();
            foreach($tagged_items[0] as $video) {
                array_unshift($videos, $CI->video_model->get_by_id($video['obj_id']));
            }
            $result['videos'] = $videos;
            return $CI->parser->parse('_partial_video',$result,TRUE);
    }

    


    

}

?>
