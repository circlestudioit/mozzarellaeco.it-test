<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MY_Controller {
	
        public function ChangeLanguage($lang){
            $this->session->set_userdata('lang', $lang);
            redirect($this->session->userdata('currentPage'));
        }
        
        public function index() {
		
            /*
             *set up title and keywords (if not the default in custom.php config file will be set) 
             */

            $this->title = "Tonda Pizza. Fiera di essere italiana. Proudly Italian.";
            $this->keywords = "";
            $this->description = "";
            $this->author = "Circle Studio";
            $this->javascript = array("libs/jquery.mobile-1.4.3.min.js", "libs/jquery.transit.min.js",
                                      "libs/jquery.flexslider-min.js", "script.js", "functions.js", "script-contatti.js");
            $this->css = array("main.css", "responsive.css", "flexslider.css"); 
            $this->fonts = array("Lato:100,300,400,700", "Cardo:100,400,700");

            if($this->session->userdata('lang') == null)
                $this->session->set_userdata('lang', $this->config->item('language'));
             
             
            $this->_render_template_contact();
        }
}