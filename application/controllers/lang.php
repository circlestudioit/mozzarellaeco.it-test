<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lang extends CI_Controller {
        

        function ChangeLanguage($lang = '') {
            $this->session->set_userdata('lang', $lang);
            $this->session->set_userdata('userlang', $lang);
            setlocale(LC_ALL, $lang);
            $curpage = str_replace("/index.php", "", $this->session->userdata('currentPage'));
            //print_r($this->session->userdata('currentPage'));
            redirect($curpage);
        }
        
}

?>
