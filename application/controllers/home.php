<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
        
        
        public function index() {
		
            /*
             *set up title and keywords (if not the default in custom.php config file will be set) 
             */

            $this->title = "Mòzzarella & Co. ";
            $this->keywords = "";
            $this->description = "";
            $this->author = "Circle Studio";
            $this->javascript = array("libs/jquery.mobile-1.4.3.min.js", "libs/jquery.transit.min.js",
                                      "libs/jquery.flexslider-min.js", "script.js", "functions.js", "libs/pace.min.js");
            $this->css = array("main.css", "responsive.css", "flexslider.css"); 
            $this->fonts = array("Lato:100,300,400,700");

            if($this->session->userdata('lang') == null)
                $this->session->set_userdata('lang', $this->config->item('language'));
             
            
            $this->_render('home');
        }
        
        public function store($storename) {
		
            /*
             *set up title and keywords (if not the default in custom.php config file will be set) 
             */

            $this->title = "Mòzzarella & Co. ";
            $this->keywords = "";
            $this->description = "";
            $this->author = "Circle Studio";
            $this->javascript = array("libs/jquery.mobile-1.4.3.min.js", "libs/jquery.transit.min.js",
                                      "libs/jquery.flexslider-min.js", "script.js", "functions.js", "libs/pace.min.js");
            $this->css = array("main.css", "responsive.css", "flexslider.css"); 
            $this->fonts = array("Lato:100,300,400,700");

            if($this->session->userdata('lang') == null)
                $this->session->set_userdata('lang', $this->config->item('language'));
             
            
            $this->_render_template_location($storename);
        }
        
}