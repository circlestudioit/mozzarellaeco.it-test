<nav class="navbar h0 col-xs-12 col-sm-12">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?=base_url()?>">
      <img src="<?php echo base_url(IMAGES.'momo-logo-white.png')?>" alt="mò mò" />
           </a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <div class="hidden-xs col-sm-2"></div>
        <ul class="nav navbar-nav col-xs-12 col-sm-9 text-center">
<!--            <li><a href="#">concept</a></li>
            <li><a href="#">locations</a></li>-->
            <ul class="col-xs-12 pull-right">
                <li class="whiteback">
                    <a class="introScript" href="https://www.moovenda.com/#!/ristoranti/roma/mo-mo-mozzarella-e-co-56cb15be28fe40814a7fa801" target="_blank">
                        <?=$this->lang->line('consegna-a-domicilio')?>
                    </a>
                </li>
                <li><a href="<?=base_url('lang/it')?>">ita</a></li>
                <li><a href="<?=base_url('lang/en')?>">eng</a></li>
            </ul>
        </ul>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>