<header id="header" class="zero"> <!-- class header-container -->
    <nav class="navbar h0 navbar-fixed-top col-xs-12 col-sm-12">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="javascript:_show_nav();">
            <img src="<?php echo base_url(IMAGES.'logo-tondo.png')?>" alt="Tonda" style="height: 100%;" />
                 </a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">LOCATION & MENU</a>
                      <ul class="dropdown-menu">
                          <?php foreach($countries as $country): ?>
                          <li>
                              <a role="button" href="javascript:openDrop('<?=strtoupper($country['country_name'])?>')">
                                  <?=strtoupper($country['country_name'])?>
                              </a>
                              <ul id="<?=strtoupper($country['country_name'])?>" style="display: none">
                                  <?php foreach($locations as $location): ?>
                                  <?php foreach($location as $place): 
                                      if($place['country_id'] == $country['id']): ?>
                                        <li>
                                            <?=$place['store_short_name']?>
                                        </li>
                                  <?php endif; endforeach; endforeach; ?>
                              </ul>
                          </li>
                          <?php endforeach; ?>
                      </ul>
                  </li>
                <li><a href="<?=base_url('food-and-drink')?>">FOOD & DRINK</a></li>
              <li class="hidden-xs">
                  <a href="<?=base_url()?>">
                      <img src="<?php echo base_url(IMAGES.'logo-tondo.png') ?>" alt="Tonda"/>
                  </a>
              </li>
              <li><a href="<?=base_url('franchising')?>">FRANCHISING</a></li>
              <li><a href="<?=base_url('contact')?>">CONTACT</a></li>

            </ul>
      <!--      <ul class="nav navbar-nav navbar-right">
              <li><a href="#">Link</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
              </li>
            </ul>-->
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>
<div class="nav-locations">
    <ul id="nav-countries">
        <?php foreach($countries as $country): ?>
        <li>
            <a href="javascript:openRestaurants('<?=$country['country_code']?>');void(0);"><?=strtoupper($country['country_name'])?></a>
        </li>
        <?php endforeach; ?>
    </ul>

    <ul id="nav-restaurants">
        
    </ul>
    <div class="xclose-locations"><a class="red" href="javascript: closeNavLocations()">x</a></div>
</div>