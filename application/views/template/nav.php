<nav class="navbar h0 col-xs-12 col-sm-12">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header col-xs-12 col-sm-2" style="display: block; z-index: 9999999">
        <a class="navbar-brand" href="<?=base_url()?>">
            <img src="<?php echo base_url(IMAGES.'momo-logo-white.png')?>" alt="mozzarella & co" />
        </a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="navbar hidden-xs">
        <ul class="nav navbar-nav col-xs-12 col-sm-10 text-center">
<!--            <li><a href="#">concept</a></li>
            <li><a href="#">locations</a></li>-->
            <ul class="col-xs-12 pull-right text-right">
                <li><a href="<?=base_url('lang/it')?>">ita</a></li>
                <li><a href="<?=base_url('lang/en')?>">eng</a></li>
            </ul>
        </ul>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>