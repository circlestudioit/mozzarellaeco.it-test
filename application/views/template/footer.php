<footer id="footer" class="footer-interne">
    <div class="hidden-xs col-sm-1 col-md-1 col-lg-2"></div>
    <div class="footer-container col-xs-12 col-sm-10 col-md-10 col-lg-8">
        <div class="col-xs-12 col-sm-4 text-center footer-info">
            <h4 class="white text-center">
                mò zzarella & co.
            </h4>
            &copy; 2016 - all rights reserved
            <br />
        </div>
        <div class="col-xs-12 col-sm-4 footer-info text-center">
            <a class="white" href="<?=base_url()?>">
                <img src="<?=base_url(IMAGES."momo-logo-white.png")?>" class="logo-footer" /></a>
        </div>
        <div class="col-xs-12 col-sm-4 text-center">
            <h4 class="white text-center">
                <a href="https://www.facebook.com/mozzarellaeco" target="_blank" class="white">seguici su facebook</a>&nbsp;|&nbsp; 
                <a href="https://www.instagram.com/momoitalianfood" target="_blank" class="white">instagram</a>
            </h4>
        </div>
        
    </div>
    <div class="hidden-xs col-sm-1 col-md-1 col-lg-2"></div>
    <div class="col-xs-12 text-center">
        <span class="lato white">DESIGN&CODE:</span>
        <a class="white lato" href="http://www.circlestudio.it" target="_blank">CIRCLE STUDIO</a>
    </div>
</footer>