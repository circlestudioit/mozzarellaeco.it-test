<div class="hidden-xs col-sm-2 col-md-3 col-lg-3"></div>
    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
        <div class="post-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php 
            setlocale(LC_TIME, strtolower($this->session->userdata('lang'))."_".strtoupper($this->session->userdata('lang')));

            $data['day'] = strftime("%d", strtotime($pagecontent['date']));
            $data['month'] = strftime("%h", strtotime($pagecontent['date']));
            $data['year'] = strftime("%Y", strtotime($pagecontent['date']));
            ?>
            <div class="post-day"><?=$data['day']?> <?=$data['month']?> <?=$data['year']?></div>
        </div>
        
        <div class="post-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="dark"><?=$pagecontent['title']?></h1>
            <?php if($pagecontent['headline'] != ''):?>
            <p class="post-headline text-left">
                <?=$pagecontent['headline']?>
            </p>
            
            <?php endif; ?>
            
            <?php if($pagecontent['image'] != ''):?>
            <a href="<?=base_url($this->config->item('post_image').$pagecontent['image'])?>" data-lightbox="image">
                <img src="<?=base_url($this->config->item('post_image').$pagecontent['image'])?>" class="post-image" /></a>
            <?php endif; ?>
            
            <img src="<?=base_url(IMAGES."separatore-puntini.png")?>" style="display: block; margin: 2em auto">
            
            <?php if($pagecontent['content'] != ''):?>
            <div class="post-content">
                <?=$pagecontent['content']?>
            </div>
                
            <?php endif; ?>
            <?php if(isset($pagecontent['attachment']) && $pagecontent['attachment'] != ''): ?>
            <a href="<?=base_url($this->config->item('post_file').$pagecontent['attachment'])?>" class="dark" target="_blank">
                <img src="<?=base_url(IMAGES."scheda_news_icon.png")?>" style="margin: 0 .5em 0 0" /><?=$this->lang->line('scarica-allegato')?></a>
            <?php endif; ?>
            <a href="<?=base_url('news')?>">
            <img src="<?=base_url(IMAGES."back-arrow.png")?>" class="back-news" /></a>
        </div>
<!--        <div class="post-container hidden-xs col-sm-2 col-md-2 col-lg-2 post-list-thumb" style="background-image: url(<?=base_url($this->config->item('post_thumb').$pagecontent['thumb'])?>)">
        </div>-->
    </div>
<div class="hidden-xs col-sm-2 col-md-3 col-lg-3"></div>