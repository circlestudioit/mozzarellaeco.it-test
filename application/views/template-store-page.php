<div class="col-xs-12" style="margin-top: 7em"></div>

<?php $this->load->view("map-initialize"); ?>

<div class="clear"></div>
<div class="hidden-xs col-sm-1 col-md-1 col-lg-2"></div>
<div class="col-xs-12 col-sm-10 col-md-10 col-lg-8" style="margin-bottom: 2em">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <h3 class="intro">
            <?=$pagecontent['store_name']?>
        </h3>
        <div class="lato dark">
            <?=$pagecontent['description']?>
        </div>
    </div>
    <div class="hidden-xs col-sm-12 col-md-6">
        <img src="<?=base_url($this->config->item('photo_thumb').$pagecontent['thumb'])?>" class="" style="width: 100%" />
    </div>
    <!--<div class="col-xs-12 col-sm-12">-->
        <img src="<?=base_url(IMAGES."ondine-marroni.png")?>" class="separator" />

        <div class="col-xs-12 col-sm-12">
        <?php if(empty($gallery)): ?>
            <?php if(!empty($pagecontent['image'])): ?>
            <img src="<?=base_url($this->config->item('photo_image').$pagecontent['image'])?>" class="restaurant-image" />
        <?php endif; else: ?>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="slider-store" class="flexslider-piatti">
                    <ul class="slides">
                        <?php foreach($gallery as $pic): ?>
                        <li>
                            <img src="<?=base_url($this->config->item('photo_image').$pic['image'])?>" class="" />
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        <?php endif; ?>
        </div>
        <img src="<?=base_url(IMAGES."ondine-marroni.png")?>" class="separator" />
        <div class="col-xs-12 col-sm-6">
            <div class="intro text-left">
                <?=$pagecontent['address']?>
            </div>
            <div class="intro text-left">
                <?=$pagecontent['description_2']?>
            </div>
            <?php if(!empty($location_services)): ?>
                <h3 class="intro red text-left">
                    <?=$this->lang->line('available-services');?>
                </h3>
                <ul class="services-list">
                    <?php foreach($location_services as $service): ?>
                    <li class="service-icon">
        <!--                <img src="<?=base_url($this->config->item('photo_thumb').$service['thumb'])?>" />-->
                        <div class="clear"></div>
                        <span class="service-title"><?=$service['title']?> <span class="red"> / </span> </span>
                    </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
            <br />
<!--            <img src="<?=base_url(IMAGES."segnetti.png")?>" class="separator" />-->
            <?php if($pagecontent['facebook_url'] != '' || $pagecontent['twitter_url'] != '' || $pagecontent['instagram_url'] != '' || $pagecontent['email_url'] != ''): ?>
            <div class="text-center">
                <h3 class="intro red text-left">
                    <?=$this->lang->line('seguici-sui-social')?>
                </h3>
                <div class="clear"></div>
                <div class="social-share-locations">
                    <?php if($pagecontent['facebook_url'] != ''): ?>
                    <a href="<?=$pagecontent['facebook_url']?>" target="_blank">
                        <img src="<?=base_url(IMAGES."facebook-icon-red.png")?>" class="social-icon-location" />
                    </a>
                    <?php endif; ?>

                    <?php if($pagecontent['twitter_url'] != ''): ?>
                    <a href="<?=$pagecontent['twitter_url']?>" target="_blank">
                        <img src="<?=base_url(IMAGES."twitter-icon-red.png")?>" class="social-icon-location" />
                    </a>
                    <?php endif; ?>

                    <?php if($pagecontent['instagram_url'] != ''): ?>
                    <a href="<?=$pagecontent['instagram_url']?>" target="_blank">
                        <img src="<?=base_url(IMAGES."instagram-icon-red.png")?>" class="social-icon-location" />
                    </a>
                    <?php endif; ?>

                    <?php if($pagecontent['email_url'] != ''): ?>
                    <a href="mailto:<?=$pagecontent['email_url']?>">
                        <img src="<?=base_url(IMAGES."email-icon-red.png")?>" class="social-icon-location" />
                    </a>
                    <?php endif; ?>
                </div>
            </div>

            <?php endif; ?>
        </div>
    <!--</div>-->
    <div class="col-xs-12 col-sm-6 map-container">
        <div id="map_canvas" class="map-canvas location-map"></div>
    </div>


</div>
<div class="hidden-xs col-sm-1 col-md-1 col-lg-2"></div>
<div class="clear"></div>
    
<div class="hidden-xs col-sm-1 col-md-1 col-lg-2"></div>
<div class="col-xs-12 col-sm-10 col-md-10 col-lg-8">
    <img src="<?=base_url(IMAGES."segnetti.png")?>" class="separator2" />
    <img src="<?=base_url(IMAGES."segnetti.png")?>" class="separator2" />
</div>
<div class="hidden-xs col-sm-1 col-md-1 col-lg-2"></div>

<div class="clear"></div>

<div class="hidden-xs col-sm-1 col-md-1 col-lg-2"></div>
<div class="col-xs-12 col-sm-10 col-md-10 col-lg-8 text-center">
    <div id="menu">
        <h2 class="introScript text-center">
        <?=$this->lang->line('full-menu');?>
        </h2>    
    </div>

    <a class="col-xs-12 col-sm-12 text-center menu-attachment" href="<?=base_url($this->config->item('attachments').$pagecontent['attachment'])?>" target="_blank">
        <br />
        <?=$this->lang->line('download-menu')?>
        <br /><br />
    </a>
</div>  
<div class="hidden-xs col-sm-1 col-md-1 col-lg-2"></div>
    
<div class="clear"></div>

<?php if(!empty($gallery_promo)): ?>
<div id="promo" class="zero">
    <div class="full-content full-bg col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: inline-block;">
        <h1 class="nexa text-center">
            <?=$gallery_promo_info['title']?>
        </h1>
        
        <div id="promo-slider" class="promo-slider">
            <ul class="slides">
                <?php foreach($gallery_promo as $pic): ?>
                <li>
                    <div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6" style="margin-bottom: 3em">
                        <div class="col-xs-12 col-sm-6 vertcenter-wrapper"> 
                            <div class="vertcenter-content">
                                <h1 class="nexa-no-sign">
                                    <?=$pic['description']?>
                                </h1>
                                <div class="cardo">
                                    <?=$pic['description_text']?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <img src="<?=base_url($this->config->item('photo_image').$pic['image'])?>" style="width: 100%" class="featured-image" />
                        </div>
                    </div>
                    <div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>

<?php endif; ?>
<div class="clear"></div>


<div class="clear"></div>
<?php if(!empty($pagecontent['description_3'])): ?>

    <div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6" style="margin: 3em 0"> 
        <div class="text-center cardo">
            <?=$pagecontent['description_3'];?>
        </div>
    </div>

    <div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>
<?php endif; ?>