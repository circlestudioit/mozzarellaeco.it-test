<script type="text/javascript">
      function initialize_map() {
          
        setupMap($(window).height()/2);
      
        var map;
        var restaurant = new google.maps.LatLng(42.442208,14.257456);
        var MY_MAPTYPE_ID = 'location_map';
        var stylez = [
          {
            featureType: 'all',
            elementType: 'all',
            stylers: [
                { "saturation": -100 }
            ]
          }
        ];

        var mapOptions = {
          zoom: 3,
          center: restaurant,
          mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
          },
          mapTypeId: MY_MAPTYPE_ID
        };

        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

        var styledMapOptions = {
          name: 'Tonda'
        };
        
        var image = '<?php echo base_url(IMAGES.'google-pin.png');?>';
            
        <?php $i = 0; foreach ($locations as $restaurants): 
            foreach($restaurants as $location):
                if($location['lat'] != '' && $location['long'] != ''):
            ?>
                    
        var marker_<?=$i?> = new google.maps.Marker({
            position: new google.maps.LatLng(<?=$location['lat'].",".$location['long']?>),
            map: map,
            title: '<?=$location['loc_name']?>',
            icon: image,
            url: '<?=base_url("location/".$location['loc_name'])?>'
        });
        
        google.maps.event.addListener(marker_<?=$i?>, 'click', function() {
            window.location.href = this.url;
        });
        
        <?php endif; $i++; endforeach; endforeach; ?>

        
        var jayzMapType = new google.maps.StyledMapType(stylez, styledMapOptions);

        map.mapTypes.set(MY_MAPTYPE_ID, jayzMapType);
        
      }
      
    google.maps.event.addDomListener(window, 'load', initialize_map);
</script>