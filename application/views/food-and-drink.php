<div id="home"></div>
<div id="slider-small" style="display: inline-block">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 flexslider">
        <ul class="slides">
            <?php foreach($gallery as $pic): ?>
            <li>
                <div class="slidesmall" style="background-image: url(<?=base_url($this->config->item('photo_image').$pic['image'])?>)" />
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<div class="hidden-xs col-sm-2"></div>
<div class="col-xs-12 col-sm-8">
    <ul class="subnav">
        <?php foreach($pagecontent["menu"] as $menu_item): ?>
        <li><a href="<?=$menu_item['url']?>" data-scroll="<?=$menu_item['url']?>"><?=$menu_item['title']?></a></li>
        <?php endforeach; ?>
    </ul> 
</div>
<div class="hidden-xs col-sm-2"></div>
<div class="clear"></div>
<div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>
<div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
    <div class="col-xs-12 col-sm-6">
        <h1 class="nexa text-left">
        <?=$pagecontent['title']?>
        </h1>
        <p>
            <?=$pagecontent['content']?>
        </p>
    </div>
    <div class="col-xs-12 col-sm-6">
        <img src="<?=base_url(IMAGES."alici_e_pachino.png")?>" class="pizzetta-fad" />
    </div>
</div>
<div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>

<div id="pizzette" class="zero">
    <img src="<?=base_url(IMAGES."prev-arrow.png")?>" class="prev-arrow-pizzette" />
    <img src="<?=base_url(IMAGES."next-arrow.png")?>" class="next-arrow-pizzette" />
    
    <div class="full-bg flexslider-featured col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-image: url(<?=base_url(IMAGES."blackboard.jpg")?>)">
        <h1 class="nexa white text-center featured-title">
            <?=$this->lang->line('pizzette')?>
        </h1>
        <ul class="slides" id="pizzette-fad">
            <?php foreach($pizzette as $pizzetta): ?>
            <li class="featured-section-content">
                <div class="hidden-xs col-sm-2 col-md-2 col-lg-2"></div>
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                    <div class="col-xs-12 col-sm-6">
                        <img src="<?=base_url($this->config->item("products_image").$pizzetta['image'])?>" class="pizzetta-slider-fad" />
                    </div>
                    <div class="col-xs-12 col-sm-6 vertcenter-wrapper"> 
                        <div class="vertcenter-content">
                            <h1 class="pizzetta-name white">
                                <?=$pizzetta['title']?>
                            </h1>
                            <div class="line-white"></div>
                            <div class="cardo white">
                                <?=$pizzetta['content']?>
                            </div>
                            <div class="line-white"></div>
                            <?php if($pizzetta['cover'] != ''): ?>
                            <img src="<?=base_url($this->config->item("products_cover").$pizzetta['cover'])?>" class="cardo ingredienti" />
                            <?php endif; ?>
                            <?php if(isset($pizzetta['available_location'])): ?>
                                <p class="cardo white">
                                    <?=$this->lang->line('available-only').$pizzetta['available_location']?>
                                </p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="hidden-xs col-sm-2 col-md-2 col-lg-2"></div>
            </li>
        <?php endforeach; ?>
        </ul>
        
        <div class="text-center nexa white" style="font-size: 2em;">
            <?=$this->lang->line('exclusive-taste')?>
        </div>
    </div>
    
</div>
                
<div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
        <?php foreach($pageChildren as $child): ?>
        <div id="<?=$child['name']?>"></div>
            <h1 class="nexa text-left">
                <img src="<?=base_url($this->config->item('pages_thumb').$child['thumb'])?>" style="margin-right: .5em; float: left" />
                <?=$child['title']?>
            </h1>
            <div>
                <?=$child['headline']?>
            </div>
            <img src="<?=base_url($this->config->item("pages_image").$child['image'])?>" class="center-gallery-image" />
            <div class="thin-line-black"></div>
            <div class="multicolumn2-nosep">
                <?=$child['content']?>
            </div>
            <div class="thin-line-black"></div>
            <div class="text-center">
                 <?=$child['content_2']?>
            </div>
            <img src="<?=base_url(IMAGES."separatore-puntini.png")?>" style="display: block; margin: 2em auto" />
        
        <?php endforeach; ?>
    </div>
<div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>

<div class="back-to-top">
    <a href="#home" data-scroll="#home">
        <img src="<?=base_url(IMAGES."back-to-top.png")?>" />
    </a>
</div>