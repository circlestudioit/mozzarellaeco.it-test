<div class="hidden-xs col-sm-2 col-md-3 col-lg-3"></div>
    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
        <h1 class="nexa text-center">
            <?=$pagecontent['title']?>
        </h1>
        <ul id="news-container">
        <?php
            foreach($posts as $post):
        ?>
            
            <li class="post col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <?php 
                    setlocale(LC_TIME, strtolower($this->session->userdata('lang'))."_".strtoupper($this->session->userdata('lang')));
                    
                    $data['day'] = strftime("%d", strtotime($post['date']));
                    $data['month'] = strftime("%h", strtotime($post['date']));
                    $data['year'] = strftime("%Y", strtotime($post['date']));
                    ?>
                    <div class="post-day"><?=$data['day']?> <?=$data['month']?> <?=$data['year']?></div>
<!--                    <span class="post-month"></span>
                    <div class="post-date-line"></div>-->
                </div>
                <div class="post-preview col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2 class="post-title">
                        <a href="<?=base_url('news/'.$post['url'])?>"><?=$post['title']?></a>
                    </h2>
                    <p class="post-headline text-center">
                        <?=$post['headline']?>
                    </p>
                    <div class="hidden-xs post-continua">
                        <a class="white" href="<?=base_url('news/'.$post['url'])?>"><?=$this->lang->line('continua')?></a>
                    </div>
                </div>
                <img src="<?=base_url(IMAGES."separatore-puntini.png")?>" style="display: block; margin: 2em auto">
<!--                <div class="highlight-content hidden-xs col-sm-4 col-md-4 col-lg-4 post-list-thumb" style="background-image: url(<?=base_url($this->config->item('post_thumb').$post['thumb'])?>)">
                </div>-->
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
<div class="hidden-xs col-sm-2 col-md-3 col-lg-3"></div>