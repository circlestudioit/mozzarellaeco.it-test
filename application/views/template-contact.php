<?php if(empty($gallery)): ?>
<div style="margin-top: 7em" class="col-xs-12 col-sm-12"></div>
<?php endif; ?>
<?php if(!empty($gallery)): ?>
<div id="slider-small" style="display: inline-block">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 flexslider">
        <ul class="slides">
            <?php foreach($gallery as $pic): ?>
            <li>
                <div class="slidesmall" style="background-image: url(<?=base_url($this->config->item('photo_image').$pic['image'])?>)" />
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>    
<?php endif; ?>
    
    
<div id="contact" class="col-xs-12 col-sm-12">
    <div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
            <h1 class="nexa text-center">
                <?=$pagecontent['title']?>
            </h1>
            <!--<img src="<?=base_url(IMAGES."separatore-puntini.png")?>" style="display: block; margin: 2em auto" />-->
            <div>
                <?=$pagecontent['content']?>
            </div>
        </div>  
    <div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>
</div>

<div id="work-with-us" class="zero">
    <div class="full-content full-bg col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-image: url(<?=base_url(IMAGES."blackboard.jpg")?>); display: inline-block">
        <h1 class="nexa white text-center">
            <?=$this->lang->line('join-our-team');?>
        </h1>
        <h4 class="cardo white text-center">
            <?=$this->lang->line('lavora-con-noi');?>
        </h4>
        <br /><br />
        <div class="hidden-xs col-sm-3"></div>
        <div class="cerchi-lavoro text-center col-xs-12 col-sm-6">
            <form id="cerchi-lavoro-form" enctype="multipart/form-data" action="/cerchi-lavoro" method="post">
                <input class="col-xs-12 col-sm-6" type="text" name="name" placeholder="<?=$this->lang->line('first-last-name')?>" required>
                <input class="col-xs-12 col-sm-6" type="text" name="email" placeholder="email:" required>
                <br />
                <input class="col-xs-12 col-sm-6" type="text" name="country" placeholder="<?=$this->lang->line('country')?>"><br />
                <div class="clear"></div>
                <p class="col-xs-12 col-sm-6" style="color: #AAA; margin: 2em 0">
                    <?=$this->lang->line('attach-cv')?>
                </p>
                <input class="col-xs-12 col-sm-6" type="file" id="upload" name="upload" style="border: 0;" />
                <div class="clear"></div>
                <input type="submit" value="<?=$this->lang->line('send')?>" style="margin: 1em auto">
                <br /><br />
            </form>
        </div>
        <div class="hidden-xs col-sm-3"></div>
    </div>
</div>

<div class="back-to-top">
    <a href="#home" data-scroll="#home">back to top</a>
</div>