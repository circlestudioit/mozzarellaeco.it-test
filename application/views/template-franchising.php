<?php $this->load->view("all-locations-map"); ?>
<div class="map-container">
    <div id="map_canvas" class="map-canvas all-locations-map"></div>
</div>

<div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>
<div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
    <ul class="subnav">
        <?php foreach($pagecontent["menu"] as $menu_item): ?>
        <li><a href="<?=$menu_item['url']?>" data-scroll="<?=$menu_item['url']?>"><?=$menu_item['title']?></a></li>
        <?php endforeach; ?>
    </ul>
    <?php if($pagecontent['thumb'] != ''): ?>
    <img src="<?=base_url($this->config->item('photo_thumb').$pagecontent['thumb'])?>" class="store-logo" />
    <?php endif; ?>

    <div class="clear"></div>
    <div id="informazioni" class="col-xs-12 col-sm-12" style="margin-bottom: 2em">
        <h1 class="nexa text-left">
            <?=$pagecontent['title']?>
        </h1>
        <div>
            <?=$pagecontent['content']?>
        </div>
    </div>
    
</div>

<div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>
<div class="clear"></div>

<div id="online-form" style="display: inline-block;">
    <div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
            <img src="<?=base_url(IMAGES."separatore-puntini.png")?>" style="display: block; margin: 2em auto" />
            
            <h1 class="nexa text-center">
            <?=$this->lang->line('modulo-online');?>
            </h1>
            
            <?php $this->load->view('online-form') ?>
            
            <img src="<?=base_url(IMAGES."separatore-puntini.png")?>" style="display: block; margin: 2em auto" />

            
        </div>  
    <div class="hidden-xs col-sm-2 col-md-2 col-lg-3"></div>
</div>

<div id="work-with-us" class="zero">
    <div class="full-content full-bg col-xs-12 col-sm-12 col-md-12 col-lg-12" style="background-image: url(<?=base_url(IMAGES."blackboard.jpg")?>); display: inline-block">
        <h1 class="nexa white text-center">
            <?=$this->lang->line('join-our-team');?>
        </h1>
        <h4 class="cardo white text-center">
            <?=$this->lang->line('lavora-con-noi');?>
        </h4>
        <br /><br />
        <div class="hidden-xs col-sm-3"></div>
        <div class="cerchi-lavoro text-center col-xs-12 col-sm-6">
            <form id="cerchi-lavoro-form" enctype="multipart/form-data" action="home/send_mail" method="post">
                <input class="col-xs-12 col-sm-6" style="color: white" type="text" name="name" placeholder="<?=$this->lang->line('first-last-name')?>" required>
                <input class="col-xs-12 col-sm-6" style="color: white" type="text" name="email" placeholder="email:" required>
                <br />
                <input class="col-xs-12 col-sm-6" style="color: white" type="text" name="country" placeholder="<?=$this->lang->line('country')?>"><br />
                <div class="clear"></div>
                <p class="col-xs-12 col-sm-6" style="color: #AAA; margin: 2em 0">
                    <?=$this->lang->line('attach-cv')?>
                </p>
                <input class="col-xs-12 col-sm-6" type="file" id="upload" name="upload" style="border: 0; color: white" />
                <div class="clear"></div>
                <input type="submit" value="<?=$this->lang->line('send')?>" style="margin: 1em auto">
                <br /><br />
            </form>
            <p id="form-success" class="text-center white">
                
            </p>
        </div>
        <div class="hidden-xs col-sm-3"></div>
    </div>
</div>

<div class="back-to-top">
    <a href="#home" data-scroll="#home">
        <img src="<?=base_url(IMAGES."back-to-top.png")?>" />
    </a>
</div>