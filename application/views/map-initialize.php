<script type="text/javascript">
      function initialize_map() {
          
        setupMap($(window).height()/2);
      
        var map;
        var restaurant = new google.maps.LatLng(<?=$pagecontent['lat']?>, <?=$pagecontent['long']?>);
        var MY_MAPTYPE_ID = 'location_map';
        var stylez = [
          {
            featureType: 'all',
            elementType: 'all',
            stylers: [
                //{ "saturation": -100 }
            ]
          }
        ];

        var mapOptions = {
          zoom: 15,
          center: restaurant,
          mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
          },
          mapTypeId: MY_MAPTYPE_ID
        };

        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

        var styledMapOptions = {
          name: '<?=$pagecontent['loc_name']?>'
        };
        
        var image = '<?php echo base_url(IMAGES.'google-pin.png');?>';
        
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(<?=$pagecontent['lat']?>, <?=$pagecontent['long']?>),
            map: map,
            title: '<?=$pagecontent['loc_name']?>',
            icon: image
        });

        
        var jayzMapType = new google.maps.StyledMapType(stylez, styledMapOptions);

        map.mapTypes.set(MY_MAPTYPE_ID, jayzMapType);
        
      }
      
      google.maps.event.addDomListener(window, 'load', initialize_map);
</script>