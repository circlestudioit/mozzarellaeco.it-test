<!-- slider container -->
<!--<div id="intro" class="slide">-->
<?php if($pagecontent['gallery'] != null): ?>
    <div class="iosSlider-interne">
        <div class="slider-interne">
            <?php foreach($pagecontent['gallery'] as $photo): ?>
            <article class="slide">
                <img src="<?=$this->config->item('slider_images').$photo['img0']?>" style="width: 100%" />
                <div class='caption'>
                    <h3 class='caption'>
                        <?=$photo['other_desc']?>
                    </h3>
                    <h2 class='caption'>
                        <?=$photo['description']?>
                    </h2>
                </div>
            </article>
            <? endforeach; ?>
        </div>
    </div>
<?php endif; ?>

<!--</div>-->
<?php if($pagecontent['gallery'] == null): ?>
    <h1 class="lacenterbatoro-title pagine-interne-title">
        <?=$pagecontent['title']?>
    </h1>
<?php else: ?>
    <h1 class="lacenterbatoro-title pagine-interne-title">
        <?=$pagecontent['title']?>
    </h1>
<?endif; ?>

<div class="pagina-interna-p">
    <?=$pagecontent['content']?>
<br /><br />
<div class="fb-share-button" data-href="<?=current_url()?>" data-type="button"></div>
</div>