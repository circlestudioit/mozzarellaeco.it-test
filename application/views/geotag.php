<script type="text/javascript">
// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.

function initGeoTag() {
    
  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
        //          lat: 51.700757,
        //          lng: -0.517188
      };
      
        $.ajax({
            type: 'POST',
            url: document.location.origin+'/locations/geolocate/', /* pageurl */
            data: pos,
            success: function(data) {
                /* Carico il contenuto del lavoro nel div */
                var pdata = $.parseJSON(data);
                $('#searching').html('<?=$this->lang->line('risto-piu-vicino')?> ').fadeIn();
                $('#choose').html(pdata.store_name+" "+pdata.store_short_name);
                $('#link').attr('href', '<?=base_url("location/")?>'+"/"+pdata.loc_name);
                $('#goto').fadeIn();
            }
        });
    
    });
    
  }
  else {
    alert("Geolocation not available");    
  }
  
    $('#nothanks').click(function() {
        $('.geotag').fadeOut();
    });

}
</script>

<div class="geotag">
    <div class="geotag-content">
        <span id="searching">
            <?=$this->lang->line('geotagging');?>
        </span>
        <br /><span id="choose"></span><br /><br />
        <span id="goto" style="display: none">
            <a class="nexalink red" id="link"><?=$this->lang->line('si-pagina-ristorante')?></a><br /><a href="#" id="nothanks" class="nexalink white"><?=$this->lang->line('no-pagina-ristorante')?></a>
        </span>

        <img class="logo-tondo" src="<?php echo base_url(IMAGES.'logo-tondo.png') ?>" alt="Tonda"/>
    </div>
</div>