<div class="zero">
    <div class="col-xs-12 col-sm-12" id="home">
        <img src="<?=base_url(IMAGES."poster.jpg")?>" class="poster" />
        <div class="benvenuto text-center">
            <h1 class="introScript benvenutoTitle">
                Benvenuto
            </h1>
            <h2 class="big intro red text-center">
                gusta la mozzarella di bufala<br>fatta al momento
            </h2>
        </div>

        <div id="video">
            <iframe id="vimeo" src="https://player.vimeo.com/video/204502636?background=1&loop=0" width="" height="" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
        <img src="<?=base_url(IMAGES."down.png")?>" class="down-arrow" />
    </div>
    <div class="col-xs-12 col-sm-12 background-red">
        <div class="hidden-xs col-sm-2">
            <img src="<?=base_url(IMAGES."segnetti-white.png")?>" class="segnetti-white-l" />
        </div>
        <div class="col-xs-12 col-sm-8" id="haimaivisto">
            <h1 class="introScript dark extrabig text-center">
                Hai mai visto produrre
            </h1>
            <h2 class="white intro text-center">
                una vera mozzarella di bufala?
            </h2>
            <h2 class="green intro text-center">
                noi la facciamo al momento.<br />
                puoi gustarla qui o portarla a casa.
            </h2>
        </div>
        <div class="hidden-xs col-sm-2">
            <img src="<?=base_url(IMAGES."segnetti-white.png")?>" class="segnetti-white-r" />
        </div>
    </div>
    <div id="sliderhome">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 flexslider">
            <ul id="sliderhome-slides-ul" class="slides">
                <?php foreach($gallery as $pic): ?>
                <li>
                    <div class="slidehome" style="background-image: url(<?=base_url($this->config->item('photo_image').$pic['image'])?>)" />
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div id="la-facciamo-qui">
        <!--<div class="hidden-xs col-sm-1"></div>-->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-6">
                    <div class="hidden-xs col-sm-1 col-md-1 col-lg-2"></div>
                    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-8" style="padding: 0 15px !important;">
                        <br /><br />
                        <h1 class="introScript red">
                            <?=$pagecontent['title']?>
                        </h1>
                        <h3 class="intro">
                            <?=$pagecontent['headline']?>
                        </h3>
                        <div class="intro text-justify">
                            <?=$pagecontent['content']?>
                        </div>
                        <img src="<?=base_url(IMAGES."segnetti.png")?>" class="ondine-hp" />
                        <div class="intro text-justify">
                            <?=$pagecontent['content_2']?>
                        </div>
                        <h3 class="introScript red">
                            Mangiarla appena fatta, è tutta un'altra cosa!
                        </h3>
                    </div>
                    <div class="hidden-xs col-sm-1 col-md-1 col-lg-2">

                    </div>

                </div>
                <div class="col-xs-12 col-sm-6">
                    <img src="<?=base_url($this->config->item('pages_image').$pagecontent['image'])?>" class="home-image" />
                    <img src="<?=base_url(IMAGES."ondine.png")?>" class="ondine" />
                </div>
                <div class="clear"></div>
                <div class="clear"></div>
                <div class="col-xs-12 col-sm-12">
                </div>
            </div>
<!--            <div class="hidden-xs col-sm-1"></div>-->
    </div>

    <div id="bufale" style="background-image: url(<?=base_url(IMAGES."bufale.jpg")?>)" class="fade">
        <div class="quote1 col-xs-12 col-sm-4 pull-left" style="padding: 0 15px !important;">
            <h1 class="introScript big green">
                Mozzarella
            </h1>
            <h3 class="intro text-justify white" style="line-height: 1.2em;">
                il termine deriva dalla lavorazione: con la "mozzatura", cioè il taglio della pasta lavorata, si ottiene la mozzarella.<br />Noi la facciamo al momento.
            </h3>
        </div>
        <div class="hidden-xs col-sm-4"></div>
        <div class="quote2 col-xs-12 col-sm-4 pull-right text-right" style="padding: 0 15px !important;">
            <h1 class="introScript big green">
                100% latte di Bufala
            </h1>
            <h2 class="intro green text-right">
                della piana dei templi di pæstum
            </h2>
        </div>

    </div>

    <div id="contatti">
        <br />
        <h1 class="introScript red text-center">
            Dove ci puoi trovare
        </h1>
        <div class="hidden-xs col-sm-2"></div>
        <div class="col-xs-12 col-sm-4">
            <div class="hidden-xs col-sm-1"></div>
            <div class="col-xs-12 col-sm-12 text-center" style="margin: 2em auto">
                <div class="infolocale">
                    <h1 class="introScript red text-center">
                        Piazza Bologna
                    </h1>
                    <div class="text-center intro dark">
                        Viale delle Provincie n. 84-86 - Roma<br />
                        Tel. 06 44 25 25 72<br />
                        <a href="https://www.facebook.com/mozzarellaeco/" target="_blank">facebook.com/mozzarellaeco</a><br />
                    </div>
                    <h3 class="introScript red text-center">
                        Orari di Apertura
                    </h3>
                    <div class="text-center intro dark">
                        dal Lunedì al Mercoledì 10:00 - 23:00<br />
                        dal Giovedì alla Domenica 10:00 - 24:00<br />
                    </div>
                    <br />
                    <a href="<?=base_url("location/roma-piazza-bologna")?>" class="vaiallostore">vai allo store</a>
                </div>
            </div>
            <div class="hidden-xs col-sm-1"></div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="hidden-xs col-sm-1"></div>
            <div class="col-xs-12 col-sm-12 text-center" style="margin: 2em auto">
                <div class="infolocale">
                    <h1 class="introScript red text-center">
                        Cola di Rienzo
                    </h1>
                    <div class="text-center intro dark">
                        Via Lucrezio Caro n. 58-60 - Roma<br />
                        Tel. 06 683 2068<br />
                        <a href="https://www.facebook.com/mozzarellaeco2/" target="_blank">facebook.com/mozzarellaeco2</a><br />
                    </div>
                    <h3 class="introScript red text-center">
                        Orari di Apertura
                    </h3>
                    <div class="text-center intro dark">
                        dal Lunedì al Mercoledì 07:30 - 23:00<br />
                        dal Giovedì alla Domenica 07:30 - 24:00<br />
                    </div>
                    <br />
                    <a href="<?=base_url("location/roma-cola-di-rienzo")?>" class="vaiallostore">vai allo store</a>
                </div>
            </div>
            <div class="hidden-xs col-sm-1"></div>
        </div>
        <div class="hidden-xs col-sm-2"></div>
    </div>
</div>