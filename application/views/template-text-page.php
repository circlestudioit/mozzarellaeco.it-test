<div class="hidden-xs col-sm-2 col-md-3 col-lg-3"></div>
    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
        
        <div class="post-container col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="dark text-center"><?=$pagecontent['title']?></h1>
            <?php if($pagecontent['headline'] != ''):?>
            <p class="post-headline text-left">
                <?=$pagecontent['headline']?>
            </p>
            
            <?php endif; ?>
            <img src="<?=base_url(IMAGES."separatore-puntini.png")?>" style="display: block; margin: 2em auto">
            <?php if($pagecontent['content'] != ''):?>
            <div class="post-content">
                <?=$pagecontent['content']?>
            </div>
            <?php endif; ?>
            
            <?php if(isset($pagecontent['attachment']) && $pagecontent['attachment'] != ''): ?>
            <a href="<?=base_url($this->config->item('post_file').$pagecontent['attachment'])?>" class="dark" target="_blank">
                <img src="<?=base_url(IMAGES."scheda_news_icon.png")?>" style="margin: 0 .5em 0 0" /><?=$this->lang->line('scarica-allegato')?></a>
            <?php endif; ?>
            
        </div>
        
    </div>
<div class="hidden-xs col-sm-2 col-md-3 col-lg-3"></div>