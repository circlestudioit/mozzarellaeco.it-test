<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $CI = & get_instance();
    }

    var $table                                      = 'products';       // Tabella madre prodotti
    var $table_i18n                                 = 'products_i18n';  // Tabella prodotti localizzata
    
    var $table_categories                           = 'products_categories'; // Tabella categoria
    var $table_categories_i18n                      = 'products_categories_i18n'; // Tabella categoria localizzata
    
    var $table_families                             = 'products_families'; // Tabella famiglia
    var $table_families_i18n                        = 'products_families_i18n'; // Tabella famiglia localizzata
    
    //var $table_relation                             = 'prodcategories_and_families'; // Tabella relazione
    var $table_relation_products_and_families       = 'products_and_families'; // Tabella relazione
    
    
    var $post_photogalleries_table = 'post_photogalleries'; // Tabella photogallery
    var $photogalleries_table = 'photogalleries_photos'; // Tabella foto delle photogallery
    var $photos_table = 'photos'; // tabella foto
    var $photos_i18n_table = 'photos_i18n'; // tabella decodifica lingua foto
    var $post_files_table = 'post_files'; // tabella dei file di un post

    
    
    /******************** CATEGORIE ***********************/
    
    public function get_category_by_name($name) 
    {
        $this->db->join($this->table_categories, $this->table_categories.'.category_id = '.$this->table_categories_i18n.'.id');
        $query = $this->db->get_where($this->table_categories_i18n, 
                array($this->table_categories_i18n.'.lang' => $this->session->userdata('lang'), $this->table_categories.'.name' => $name));
        $result = $query->result_array();
        return $result[0];
    }
    
    public function get_categories() 
    {
        $this->db->order_by("ord", "ASC");
        $this->db->join($this->table_categories, $this->table_categories.'.category_id = '.$this->table_categories_i18n.'.id');
        $query = $this->db->get_where($this->table_categories_i18n, array($this->table_categories_i18n.'.lang' => $this->session->userdata('lang')));
        return $query->result_array();
    }
    
    
    /******************** CATEGORIE ***********************/
    
    
    
    /******************** FAMIGLIE ***********************/
    
    public function get_family_by_name($family) {
        
        $this->db->join($this->table_families, $this->table_families.'.family_id = '.$this->table_families_i18n.'.id');
        $query = $this->db->get_where($this->table_families_i18n, 
                array($this->table_families_i18n.'.lang' => $this->session->userdata('lang'), $this->table_families.'.family_name' => $family));
        $result = $query->result_array();
        return $result[0];
    }
    
    public function get_families()
    {
        $this->db->order_by("ord", "ASC");
        $this->db->join($this->table_families, $this->table_families.'.family_id = '.$this->table_families_i18n.'.id');
        $query = $this->db->get_where($this->table_families_i18n, array($this->table_families.'.is_public' => false, $this->table_families.'.is_published' => true, $this->table_families_i18n.'.lang' => $this->session->userdata('lang')));
        return $query->result_array();
    }
    
    public function get_families_for_category($category_name)
    {
        
        $this->db->select($this->table_categories.".*, ".$this->table_families.".*, ".$this->table_categories_i18n.".*, ".$this->table_families_i18n.".*");
        $this->db->from($this->table_categories);
        
        $this->db->join($this->table_relation, $this->table_relation.'.category_id = '.$this->table_categories.'.category_id');
        $this->db->join($this->table_families, $this->table_families.'.family_id = '.$this->table_relation.'.family_id');
        
        $this->db->join($this->table_families_i18n, $this->table_families_i18n.'.id = '.$this->table_families.'.family_id');
        $this->db->join($this->table_categories_i18n, $this->table_categories_i18n.'.id = '.$this->table_categories.'.category_id AND '.$this->table_categories_i18n.'.lang = "'.$this->session->userdata('lang').'"');
        
        $this->db->where(array($this->table_categories.'.name' => $category_name, $this->table_families.'.is_public' => false, $this->table_families.'.is_published' => true));
        
        $this->db->order_by($this->table_relation.".priority", "ASC");
        
        $query = $this->db->get();

        $result = $query->result_array();
        if($result != null) {
            return $result;
        }
        else return array();
        
    }
    
    public function get_family_by_id($family_id) {
        $this->db->join($this->table_families_i18n, $this->table_families_i18n.'.id = '.$this->table_families.'.family_id');
        $this->db->order_by('ord', 'ASC');
        $query = $this->db->get_where($this->table_families, array('family_id' => $family_id, $this->table_families_i18n.'.lang' => $this->session->userdata('lang')));
        $result = $query->result_array();
        if($result != null) {
            return $result[0];
        }
        else return array();
    }
    
    public function get_family_id_from_name($family) {
        $this->db->select('family_id');
        $query = $this->db->get_where($this->table_families, array($this->table_families.".family_name" => $family));
        $result = $query->result_array();
        if($result != null) {
            return $result[0]['family_id'];
        }
        else return array();
    }
    
    public function get_products_for_family($family) {
        
        $family_id = $this->get_family_id_from_name($family);
            
        $this->db->join($this->table_i18n, $this->table_i18n.'.product_id = '.$this->table.'.product_id');
        $this->db->order_by($this->table.'.ord', 'ASC');
        $this->db->where(array($this->table.'.family_id' => $family_id, $this->table.'.published' => 1, $this->table_i18n.'.lang' => $this->session->userdata('lang') ));
        $query = $this->db->get($this->table);
        
        $result = $query->result_array();
        $ret = array();
        
        foreach($result as $item) {
            if($item['available_in'] != 0)
                $item['available_location'] = $this->locations_model->get_country_name_by_id($item['available_in']);
            
            array_push($ret, $item);
        }
        
        if($result != null) {
            return $ret;
        }
        else {
            return array();
        }
    }
    
    /******************** FAMIGLIE ***********************/

    
    /******************** PRODOTTI ***********************/
    
    public function get_product_full_content($product) {
        $this->db->join($this->table_i18n, $this->table_i18n.'.product_id = '.$this->table.'.product_id');
        $query = $this->db->get_where($this->table, array($this->table.'.product_name' => $product, $this->table_i18n.'.lang' => $this->session->userdata('lang')));
        
          if($query->result_array() != null) {
            $result = $query->result_array();    
            return $result[0];
        }
        else return array();
    }
    
    public function get_product_full_content_by_id($product_id) {
        $this->db->join($this->table_i18n, $this->table_i18n.'.product_id = '.$this->table.'.product_id');
        $query = $this->db->get_where($this->table, array($this->table.'.product_id' => $product_id, $this->table_i18n.'.lang' => $this->session->userdata('lang')));
        
          if($query->result_array() != null) {
            $result = $query->result_array();    
            return $result[0];
        }
        else return array();
    }
    
    public function get_all_products() {
        
        $families = $this->get_families();
        $all_products = array();
        
        foreach($families as $family) {
            $family['family_name'] = $family['family_name'];
            $family['products'] = $this->get_products_for_family_by_id($family['id']);
            array_push($all_products, $family);
        }
        
        if(!empty($all_products)) {
            return $all_products;
        }
        else {
            return array();
        }
    }
    
    /******************** PRODOTTI ***********************/
    
    public function get_id_by_name($name) {
        $query = $this->db->get_where($this->table, array('product_name' => $name), 1, 0);
        return $query->row()->id;
    }

    public function get_by_id($id) {
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row_array();
    }
    
    public function get_id_by_name_and_family($product, $family) {
        $family_id = $this->get_family_id_from_name($family);
        $query = $this->db->get_where($this->table, array('product_name' => $product, 'category_id' => $family_id), 1, 0);
        return $query->row_array();
    }

    public function count() {
        $this->db->select('count(id) as record_count')->from($this->table);

        $record = $this->db->get();
        $row = $record->row();

        return $row->record_count;
    }
    
    public function get_category_by_id($category_id) {
        
        $query = $this->db->get_where($this->table_categories, array('category_id' => $category_id));
        $result = $query->result_array();
        if($result != null) {
            return $result[0];
        }
        else {
            return array();
        }
    }
   
    
    public function get_products_for_family_by_id($family_id) {
        
        $this->db->join($this->table_i18n, $this->table_i18n.'.product_id = '.$this->table.'.product_id');
        $this->db->order_by("ord", 'ASC');
        $query = $this->db->get_where($this->table, array('category_id' => $family_id, $this->table_i18n.'.lang' => $this->session->userdata('lang')));
        $result = $query->result_array();
        if($result != null) {
            return $result;
        }
        else {
            return array();
        }
        
//        $query = $this->db->query("
//          SELECT ".$this->table.".* 
//          FROM ".$this->table.
//          " WHERE ".$this->table.".category_id = '".$family_id."'"
//          );
//        
//          if($query->result_array() != null) {
//            return $query->result_array();
//            }
//        else return array();
    }
    
    
}

?>