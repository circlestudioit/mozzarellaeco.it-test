<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Post_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $CI = & get_instance();
    }

    var $table                              = 'post';  // Tabella madre
    var $table_i18n                         = 'post_i18n';  // Tabella lingue
    var $table_categories                   = 'post_categories'; // Tabella categoria
    var $post_photogalleries_table          = 'post_photogalleries'; // Tabella photogallery
    var $photogalleries_table               = 'photogalleries_photos'; // Tabella foto delle photogallery
    var $photos_table                       = 'photos'; // tabella foto
    var $photos_i18n_table                  = 'photos_i18n'; // tabella decodifica lingua foto
    var $post_files_table                   = 'post_files'; // tabella dei file di un post

    
    public function get_id_by_name($name) {
        $query = $this->db->get_where($this->table, array('post_name' => $name), 1, 0);
        return $query->row()->id;
    }

    public function get_by_id($id) {
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row_array();
    }

    public function get_lang_by_id($id,$lang) {
        $query = $this->db->get_where($this->table_i18n, array('id' => $id,'lang' =>$lang), 1, 0);
        return $query->row_array();
    }

    public function get_news_gallery($news_id) {
        $query = $this->db->query("
           SELECT photogallery_id FROM ".$this->post_photogalleries_table." 
           WHERE ".$this->post_photogalleries_table.".post_id = ".$news_id
        );
        
        $ret = (!empty($query->row()->photogallery_id)) ? $query->row()->photogallery_id : "" ;
        return $ret;
    }

    public function count() {
        $this->db->select('count(id) as record_count')->from($this->table);

        $record = $this->db->get();
        $row = $record->row();

        return $row->record_count;
    }
    
    public function get_all_posts() {
        $this->db->where($this->table.'.date <= CURDATE()"');
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
    
    public function get_post_by_url($url) {
        $this->db->select('*');
        $this->db->join($this->table_i18n, $this->table.".id = ".$this->table_i18n.".id");

        $query = $this->db->get_where($this->table, array($this->table.'.url' => $url, $this->table_i18n.'.lang' => $this->session->userdata('lang')), 1, 0);
        return $query->row_array();
    }
    
    public function get_highlight_post() {
        $this->db->select('*');
        $this->db->join($this->table_i18n, $this->table.".id = ".$this->table_i18n.".id");

        $query = $this->db->get_where($this->table, array($this->table.'.highlight' => 1, $this->table_i18n.'.lang' => $this->session->userdata('lang')), 1, 0);
        return $query->row_array();
    }
    
    public function get_category_id_by_name($category) {
        $query = $this->db->get_where($this->table_categories, array('name' => $category));
        $result = $query->result();
        return $result[0]->id;
    }
    
    public function get_categories() {
        $query = $this->db->get($this->table_categories);
        return $query->result_array();
    }
    
    public function get_posts_for_category($category) {
        
        $query = $this->db->get_where($this->table_categories, array('name' => $category));
        $category_id = $query->result_array();
        if(!empty($category_id)) {
            $category_id = $category_id[0];
            $this->db->join($this->table_i18n, $this->table.'.id = '.$this->table_i18n.'.id');
            $this->db->where($this->table.'.date <= CURDATE()');
            $query2 = $this->db->get_where($this->table, array($this->table.'.published' => true, 'category_id' => $category_id['id'], $this->table_i18n.'.lang' => $this->session->userdata('lang')));
            return $query2->result_array();
        }
        else return array();
    }
    
    public function get_all_posts_excluding_category($category) {
        
        $cat_id = $this->get_category_id_by_name($category);
        $this->db->where_not_in('id', $cat_id[0]);
        $this->db->select('id');
        $query = $this->db->get($this->table_categories);
        $category_id = $query->result_array();
        $categories = array();
        foreach($category_id as $item) {
            array_push($categories, $item['id']);
        }
        
        if(!empty($categories)) {
            $this->db->where_in('category_id', $categories);
            $this->db->order_by('highlight DESC, date DESC');
            $this->db->join($this->table_i18n, $this->table.'.id = '.$this->table_i18n.'.id');
            $this->db->where($this->table.'.date <= CURDATE()');
            $query2 = $this->db->get_where($this->table, array($this->table.'.published' => TRUE, $this->table_i18n.'.lang' => $this->session->userdata('lang')));
            
            return $query2->result_array();
        }
        else return array();
    }
    
    public function get_posts_for_category_id($category_id) {
        $this->db->join($this->table_i18n, $this->table.'.id = '.$this->table_i18n.'.id');
        $this->db->where($this->table.'.date <= CURDATE()');
        $query = $this->db->get_where($this->table, array($this->table.'.published' => true, 'category_id' => $category_id, $this->table_i18n.'.lang' => $this->session->userdata('lang')));
        return $query->result_array();
    }
    
    public function get_random_quote($category) {
        $query = $this->db->get_where($this->table_categories, array('name' => $category));
        $category_id = $query->result_array();
        if(!empty($category_id)) {
            $category_id = $category_id[0];
            $this->db->join($this->table_i18n, $this->table.'.id = '.$this->table_i18n.'.id');
            $this->db->where($this->table.'.date <= CURDATE()');
            $this->db->order_by($this->table.'.date', 'random');
            $query2 = $this->db->get_where($this->table, array($this->table.'.published' => true, 'category_id' => $category_id['id'], $this->table_i18n.'.lang' => $this->session->userdata('lang')), 1,0);
            return $query2->result_array();
        }
        else return array();
    }

}

?>