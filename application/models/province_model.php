<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Province_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $CI = & get_instance();
    }

    var $table = 'province';  // modify here
   
    
    public function get_by_id($id) {
       /* $query = $this->db->query("
        SELECT
        ");*/
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row_array();
    }
    
    public function get_id_by_name($name) {
       /* $query = $this->db->query("
        SELECT
        ");*/
        $query = $this->db->get_where($this->table, array('name' => $name), 1, 0);
        return $query->row()->id;
    }
    
    public function get_name_by_id($id) {
        $query = $this->db->get_where($this->table, array('id' => $id), 1, 0);
        return $query->row()->name;
    }

   
    public function count() {
        $this->db->select('count(id) as record_count')->from($this->table);

        $record = $this->db->get();
        $row = $record->row();

        return $row->record_count;
    }

    public function get_province() {

        $query = $this->db->query("
          SELECT * FROM ".$this->table.
          " ORDER BY ".$this->table.".sigla ASC");
        
        return $query->result_array();
    }

    
}

?>