<?php

$lang['about']                      = "About";
$lang['the-location']               = "Il Locale";
$lang['available-services']         = "Servizi disponibili";
$lang['location-specifics']         = "Dove & Quando";
$lang['opening-hour-and-contact']   = "orari e contatti";
$lang['where-we-are']               = "dove siamo";
$lang['lets-socialize']             = "Socializziamo!";
$lang['full-menu']                  = "Il nostro menu";
$lang['promo']                      = "Promo";
$lang['join-our-team']              = "Lavora con noi";

$lang['consegna-a-domicilio']       = "Consegna a domicilio";
$lang['seguici-sui-social']         = "Seguici sui Social";

$lang['modulo-online']              = "Riempi il modulo online";


$lang['download-menu']                  = "Scarica il Menu in PDF";

$lang['first-last-name']                = "Nome e Cognome";
$lang['country']                        = "Nazione";
$lang['attach-cv']                      = "Allega il Curriculum Vitae";
$lang['send']                           = "Invia";

?>
