<?php

$lang['about']                      = "About";
$lang['the-location']               = "Il Locale";
$lang['available-services']         = "Servizi disponibili";
$lang['location-specifics']         = "Dove & Quando";
$lang['opening-hour-and-contact']   = "ORARI<br />& CONTATTI";
$lang['where-we-are']               = "DOVE<br />SIAMO";
$lang['lets-socialize']             = "¡Socializamos!";
$lang['full-menu']                  = "Nuestro menu";
$lang['promo']                      = "Promo";
$lang['join-our-team']              = "Trabaja con nosotros";
$lang['lavora-con-noi']             = "Se hai voglia di entrare nel nostro mondo per lavorare con noi, scrivici!";
$lang['geotagging']                 = "Stiamo cercando il ristorante più vicino a te...";
$lang['risto-piu-vicino']           = "Sei vicino a ";
$lang['si-pagina-ristorante']       = "Portami alla Pagina del locale!";
$lang['no-pagina-ristorante']       = "No grazie, voglio andare alla Home del sito";

$lang['location-coming-soon']       = "Questa pizzeria aprirà presto!";
$lang['location-coming-info']       = "Torna a trovarci per avere informazioni.<br />Seguici su Facebook e Twitter per essere aggiornato!";

$lang['news-about-tonda']           = "Notizie";

$lang['download-menu']              = "Scarica il Menu in PDF";


$lang['exclusive-taste']            = "¡Descubre el gusto exclusivo de cada nación en el menú de tu pizzería!";
$lang['featured-pizza']             = "Il gusto esclusivo";
$lang['solo-in-questo-locale']      = "Solo in questo locale puoi trovare la pizzetta...";

$lang['farina1']                    = "... nei nostri impasti a lievitazione naturale,";
$lang['farina2']                    = "non sono presenti grassi animali...";

$lang['pizzette']                   = "Pizzette";

$lang['modulo-online']              = "Rellena el formulario online";

$lang['dati-anagrafici']                = "Datos anagráficos";
$lang['per-la-tua-pizzeria']            = "Para tu pizzeria";
$lang['cognome']                        = "apellidos";
$lang['nome']                           = "nombre";
$lang['sesso']                          = "sexo";
$lang['eta']                            = "edad";
$lang['titolo-di-studio']               = "estudios realizados";
$lang['azienda']                        = "empresa";
$lang['ragione-sociale']                = "raz&oacute;n social";
$lang['nazione-di-interesse']           = "¿En qué país?";
$lang['provincia-di-interesse']         = "Si es en Italia, ¿en qué provincia?";
$lang['cap']                            = "CAP";
$lang['cellulare']                      = "tel. cellulare";
$lang['telefono']                       = "telefono";
$lang['email']                          = "e-mail";
$lang['disponibilita-locale']           = "¿Dispones de un local?";
$lang['si']                             = "sì";
$lang['no']                             = "no";
$lang['indirizzo']                      = "dirección";
$lang['citta']                          = "ciudad";
$lang['provincia']                      = "Provincia / Pa&iacute;s";
$lang['modalita-finanziamento']         = "tipo de financiación para la inversión";
$lang['mezzi-finanziari-propri']        = "Autofinanciamiento";
$lang['mezzi-finanziari-terzi']         = "Financiamiento externo (banco, sociedad financiaria, leasing, etc.)";
$lang['orari-contatto']                 = "Orario en el que deseas ser contactado";
$lang['mattina']                        = "por la mañana";
$lang['pomeriggio']                     = "por la tarde";
$lang['sera']                           = "por la noche";
$lang['documenti-da-allegare']          = "Documentos que debes adjuntar";
$lang['doc-aziende']                    = "- Para empresas: Company Profile;";
$lang['doc-investitori']                = "- Para empresarios privados: Curriculum Vitae";
$lang['upload']                         = "Elije documento";
$lang['note']                           = "Notas";
$lang['privacy']                        = "privacy";
$lang['privacy-txt']                    = "Los datos personales facilitados serán manejados dentro de los parámetros de la política de privacidad y utilizados exclusivamente por la empresa Trieste sas., con el único objetivo de informar a cerca de sus productos y servicios. En cualquier momento será posible dirigirse a la empresa, enviando un email a privacy@trieste sas.com , o telefoneando al numero + 39 085 421 40 38 (Lun-Ven. orario 08:00 - 20:00), para acceder, rectificar, o retirar el permiso al tratamiento de datos tal y como dispone la política de privacidad.";
$lang['privacy-dichiaro']               = "Declaro haber leído la política de privacidad y acepto el tratamiento de los datos ´personales facilitados conforme con la ley 196/2003 y sus sucesivas modificaciones e ampliaciones.";
$lang['invia-richiesta']                = "enviar";
$lang['grazie-per-aver-inviato']        = "Grazie per aver inviato la tua richiesta.";

$lang['first-last-name']                = "Nombre y Apellidos";
$lang['country']                        = "País";
$lang['attach-cv']                      = "Curriculum Vitae";
$lang['send']                           = "Enviar";

$lang['tutti-i-marchi']                 = "Tutti i Marchi Tonda nel mondo sono di proprietà esclusiva di Trieste di Ciferni Riccardo &C. sas";

$lang['available-only']                 = "* Questa pizzetta è disponibile solo in ";

?>
