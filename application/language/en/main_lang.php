<?php

$lang['about']                      = "About";
$lang['the-location']               = "The Restaurant";
$lang['available-services']         = "Available services";
$lang['location-specifics']         = "Dove & Quando";
$lang['opening-hour-and-contact']   = "orari e contatti";
$lang['where-we-are']               = "dove siamo";
$lang['lets-socialize']             = "Socializziamo!";
$lang['full-menu']                  = "Our menu";
$lang['promo']                      = "Promo";
$lang['join-our-team']              = "Lavora con noi";

$lang['consegna-a-domicilio']       = "Home delivery";
$lang['seguici-sui-social']         = "Follow us on Social";

$lang['modulo-online']              = "Riempi il modulo online";


$lang['download-menu']              = "Download the Menu in PDF";

$lang['first-last-name']            = "Nome e Cognome";
$lang['country']                    = "Nazione";
$lang['attach-cv']                  = "Allega il Curriculum Vitae";
$lang['send']                       = "Invia";

?>
