<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
    //Page info
    protected $data = Array();
    protected $pageName = FALSE;
    protected $template = "main";
    protected $hasNav = TRUE;
    //Page contents
    protected $javascript = array();
    protected $css = array();
    protected $fonts = array();
    //Page Meta
    protected $title = FALSE;
    protected $description = FALSE;
    protected $keywords = FALSE;
    protected $author = FALSE;
	
    function __construct()
    {

            parent::__construct();
//            $this->data["uri_segment_1"] = $this->uri->segment(1);
//            $this->data["uri_segment_2"] = $this->uri->segment(2);
            $this->title = $this->config->item('site_title');
            $this->description = $this->config->item('site_description');
            $this->keywords = $this->config->item('site_keywords');
            $this->author = $this->config->item('site_author');
            
            //$this->session->set_userdata('currentPage', current_url());

            $supportedLangs = array('en', 'it', 'es');
            $languages = explode(',',substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));
                
            if(!$this->session->userdata('userlang')) {
                foreach($languages as $lang)
                {
                    if(in_array($lang, $supportedLangs))
                    {
                    // Set the page locale to the first supported language found
                    $this->session->set_userdata('lang', $lang);
                    setlocale(LC_ALL, $lang);
                    break;

                    }
                    else {
                        //Imposto la lingua di default
                        $this->session->set_userdata('lang', $this->config->item('language'));
                        setlocale(LC_ALL, $this->config->item('language'));
                    }
                }
            }

            $this->pageName = strToLower(get_class($this));

            $this->load->library('form_validation');
            
            $this->lang->load('main', $this->session->userdata('lang'));
            $this->lang->load('menu', $this->session->userdata('lang'));

            $this->load->model('page_model');
            $this->load->model('gallery_model');
            $this->load->model('post_model');
            $this->load->model('products_model');
            $this->load->model('locations_model');
            $this->load->model('menu_model');
            $this->load->model('province_model');
            
    }
        
        
    /****************************
     * 
     *           HOME
     * 
     ***************************/
        
        
    protected function _render($view) {
        
        $this->session->set_userdata('currentPage', current_url());
        //print_r($this->session->userdata('currentPage'));
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        //meta
        $toTpl["title"] = $this->title;
        $toTpl["author"] = $this->author;
        
        
        $this->template = "main";        
        
        $toTpl["pagecontent"] = $this->page_model->get_page_full_content($view);
        
//        $quotes = $this->post_model->get_random_quote('loading-quotes');
//        
//        //$this->session->set_userdata('quotes', $quotes);
//        $toBody['loading_quotes'] = $quotes;
        
        
        /* MENU */
        $toTpl["pagecontent"]["menu"] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);
        
//        $res = $this->getPageChildren($view, $toTpl);
//        $toTpl['pageChildren'] = $res['children'];
        //$toTpl['submenu'] = $res['submenu'];
        
        $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
     
        /* Carico la galleria associata alla pagina */            
        $toTpl['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);
        
        /* Carico la galleria associata alla pagina */            
        $toTpl['gallery_piatti'] = $this->gallery_model->get_photos_by_gallery_id(33);
        
        /* Featured gallery */
//        $toTpl['featured_gallery'] = $this->gallery_model->get_featured_gallery();
//        $toTpl['featured_galler_photos'] = $this->gallery_model->get_featured_gallery_photos();
        
        /* Carico il post in evidenza */
        $toTpl['pagecontent']['highlight-post'] = $this->post_model->get_highlight_post();
        
        //LOCATIONS
        //$toHeader["countries"] = $this->locations_model->get_countries();
        
        /* All Countries*/
//        $all_countries = $this->locations_model->get_all_countries();
//        //$toTpl["all_countries"] = $all_countries;
//        $toHeader["countries"] = $all_countries;
        
                
//        $locations = array();
//        foreach($toTpl["all_countries"] as $country) {
//            
//            array_push($locations, $this->locations_model->get_restaurants_for_country($country['country_code']));
//        }
//        
//        $toTpl["locations"] = $locations;
        //$toHeader["locations"] = $locations;
        //data
        $toBody["content_body"] = $this->load->view($view,array_merge($this->data,$toTpl),true);

        //nav menu
        if($this->hasNav) {
                $this->load->helper("nav");
                $toMenu["pageName"] = $this->pageName;
                $toHeader["nav"] = $this->load->view("template/nav",$toMenu,true);
        }
        $toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",'',true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


        //render view
        $this->load->view("template/skeleton",$toTpl);

    }
        
    
    
    /***************************
    * 
    *         LOCATIONS
    * 
    ***************************/
    
    protected function _render_template_location($location) {
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        $this->template = "main";

        //meta
        $toTpl["author"] = $this->author;

        $this->session->set_userdata('currentPage', current_url());
        
//        print_r($this->session->userdata('currentPage'));

        $toTpl["pagecontent"] = $this->locations_model->get_location_full_content($location);

        /* Imposto il template della pagina */
        $template = 'template-store-page';
        
        /* MENU */
        //$toTpl["pagecontent"]["menu"] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);
        
        /* Loading quotes */
        //$toBody['loading_quotes'] = $this->post_model->get_random_quote('loading-quotes');
        
        /* Meta */
        $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
        $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
        /* Carico la galleria del ristorante associata alla pagina */            
        $toTpl['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);
        
        /* Featured pizzetta */
        $toTpl['featured_pizzetta'] = $this->products_model->get_product_full_content_by_id($toTpl["pagecontent"]['featured_pizzetta']);
       
        /* Carico la galleria PROMO */
        $toTpl['gallery_promo_info'] = $this->gallery_model->get_gallery_info($toTpl["pagecontent"]['extra_photogallery_id']);
        $toTpl['gallery_promo'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['extra_photogallery_id']);
        
        /* Carico la galleria STAFF */
        $toTpl['gallery_staff_info'] = $this->gallery_model->get_gallery_info($toTpl["pagecontent"]['staff_photogallery_id']);
        $toTpl['gallery_staff'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['staff_photogallery_id']);
        
        /* Titolo barra del browser */
        $toTpl["title"] = $toTpl["pagecontent"]['store_name']." - ".$this->title;
        
        /* Tipologia post associati a questa pagina */
        //$toTpl['posts'] = $this->post_model->get_posts_for_category_id($toTpl["pagecontent"]['post_type']);
        
        //LOCATIONS
        /* Ritorna i paesi in cui ci sono locations*/
        $all_countries = $this->locations_model->get_all_countries();
        //$toTpl["all_countries"] = $all_countries;
        $toHeader["countries"] = $all_countries;
        
        /* Servizi presenti nella location */
        $toTpl["location_services"] = $this->locations_model->get_services_for_location($toTpl["pagecontent"]['loc_name']);

        $menu = array();

        if($toTpl["pagecontent"]['description'] != '')
            array_push($menu, $this->menu_model->get_by_name('about'));
        
        if(!empty($toTpl["location_services"]))
            array_push($menu, $this->menu_model->get_by_name('services'));
        
        if($toTpl["pagecontent"]['description_2'] != '')
            array_push($menu, $this->menu_model->get_by_name('contact'));
        
        if($toTpl["pagecontent"]['menu_image'] != '')
            array_push($menu, $this->menu_model->get_by_name('menu'));
        
        if($toTpl["pagecontent"]['extra_photogallery_id'] != 0)
            array_push($menu, $this->menu_model->get_by_name('promo'));
        
        if($toTpl["pagecontent"]['staff_photogallery_id'] != 0)
            array_push($menu, $this->menu_model->get_by_name('staff'));
        
        $toTpl["pagecontent"]["menu"] = $menu;
        
        //print_r($toTpl["pagecontent"]);
        
        //data
        $toBody["content_body"] = $this->load->view($template,array_merge($this->data,$toTpl),true);

        $toHeader["nav"] = $this->load->view("template/nav",'',true);
        //$toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",'',true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


        //render view
        $this->load->view("template/skeleton",$toTpl);

    }
    
    
    /***************************
    * 
    *     TEMPLATE CONTACT
    * 
    ***************************/
        
    protected function _render_template_contact() {
        
        $last = $this->uri->total_segments();
        $page_name = $this->uri->segment($last);
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        $this->template = "main";

        //meta
        $toTpl["author"] = $this->author;

        $this->session->set_userdata('currentPage', current_url());

        $toTpl["pagecontent"] = $this->page_model->get_page_full_content($page_name);

        /* Imposto il template della pagina */
        $template = $toTpl["pagecontent"]['template_name'];
        
        /* Loading quotes */
        $toBody['loading_quotes'] = $this->post_model->get_random_quote('loading-quotes');
        
        /* Meta */
        $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
        $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
        /* Carico la galleria associata alla pagina */            
        $toTpl['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);

        //$toTpl['big_slider'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['extra_photogallery_id']);

        /* Titolo barra del browser */
        $toTpl["title"] = $toTpl["pagecontent"]['page_title']." - ".$this->title;
        
        /* Tipologia post associati a questa pagina */
        //$toTpl['posts'] = $this->post_model->get_posts_for_category_id($toTpl["pagecontent"]['post_type']);

        /* Breadcrumb nella pagina */
        //$toTpl["breadcrumb_title"] = $toTpl["pagecontent"]['title'];
        
        /* SOTTO-MENU PAGINA */
        //$toTpl['children'] = $this->getPageChildren($page_name, $toTpl);
        
        //LOCATIONS
        $all_countries = $this->locations_model->get_all_countries();
        $toHeader["countries"] = $all_countries;

        //data
        $toBody["content_body"] = $this->load->view($template,array_merge($this->data,$toTpl),true);


        $toHeader["nav"] = $this->load->view("template/nav",'',true);
        //$toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",'',true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


        //render view
        $this->load->view("template/skeleton",$toTpl);

    }
    
    /***************************
    * 
    *     TEMPLATE SUBPAGE
    * 
    ***************************/
    
    protected function _render_template_subpage() {
        
        $last = $this->uri->total_segments();
        $page_name = $this->uri->segment($last-1);
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        $this->template = "main";

        //meta
        $toTpl["author"] = $this->author;

        $this->session->set_userdata('currentPage', current_url());

        $toTpl["pagecontent"] = $this->page_model->get_page_full_content($page_name);

        /* Imposto il template della pagina */
        $template = $toTpl["pagecontent"]['template_name'];
        
        /* Meta */
        $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
        $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
        /* Carico la galleria associata alla pagina */            
        //$toTpl["pagecontent"]['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);

        //$toTpl['big_slider'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['extra_photogallery_id']);

        /* Titolo barra del browser */
        $toTpl["title"] = $toTpl["pagecontent"]['page_title']." - ".$this->title;
        
        /* Tipologia post associati a questa pagina */
        $toTpl['posts'] = $this->post_model->get_posts_for_category_id($toTpl["pagecontent"]['post_type']);

        /* Breadcrumb nella pagina */
        //$toTpl["breadcrumb_title"] = $toTpl["pagecontent"]['title'];
        
        /* SOTTO-MENU PAGINA */
        $submenu = array();
        
        /* RECUPERO TUTTI I FIGLI DELLA PAGINA */
        $children_array = $this->page_model->get_page_children_of($this->page_model->get_id_by_name($page_name));
        $children = array();
        foreach($children_array as $child) {
            $childcontent = $this->page_model->get_page_full_content($child['name']);
            array_push($children, $childcontent);
            if($child['on_menu']) {
                array_push($submenu, $childcontent);
            }
        }
        $toTpl['children'] = $children;
        $toTpl['submenu'] = $submenu;

        //data
        $toBody["content_body"] = $this->load->view($template,array_merge($this->data,$toTpl),true);


        $toHeader["nav"] = $this->load->view("template/nav",'',true);
        //$toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",'',true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


        //render view
        $this->load->view("template/skeleton-subpage",$toTpl);

    }
    
   /******************************
    * 
    *    TEMPLATE NEWS & PRESS
    * 
    ******************************/
    
    protected function _render_template_news() {
        
        $last = $this->uri->total_segments();
        $page_name = $this->uri->segment($last);
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        $this->template = "main";

        //meta
        $toTpl["author"] = $this->author;

        $this->session->set_userdata('currentPage', current_url());

        $toTpl["pagecontent"] = $this->page_model->get_page_full_content($page_name);

        /* Imposto il template della pagina */
        $template = $toTpl["pagecontent"]['template_name'];
        
        /* MENU */
        $toTpl["pagecontent"]["menu"] = $this->menu_model->get_menu_items($toTpl["pagecontent"]['menu_id']);
        
        //LOCATIONS
        /* Ritorna i paesi in cui ci sono locations*/
        $all_countries = $this->locations_model->get_all_countries();
        $toHeader["countries"] = $all_countries;
        
        $toBody['loading_quotes'] = $this->post_model->get_random_quote('loading-quotes');
        
        /* Meta */
        $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
        $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
        /* Carico la galleria associata alla pagina */            
        //$toTpl["pagecontent"]['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);

        //$toTpl['big_slider'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['extra_photogallery_id']);

        /* Titolo barra del browser */
        $toTpl["title"] = $toTpl["pagecontent"]['page_title']." - ".$this->title;
        
        /* Tipologia post associati a questa pagina */
        $toTpl['posts'] = $this->post_model->get_all_posts_excluding_category('loading-quotes');

        /* Breadcrumb nella pagina */
        //$toTpl["breadcrumb_title"] = $toTpl["pagecontent"]['title'];
        
        /* SOTTO-MENU PAGINA */
        $res = $this->getPageChildren($page_name, $toTpl);
        $toTpl['children'] = $res['children'];
        $toTpl['submenu'] = $res['submenu'];

        //data
        $toBody["content_body"] = $this->load->view($template,array_merge($this->data,$toTpl),true);


        $toHeader["nav"] = $this->load->view("template/nav",'',true);
        //$toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",'',true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


        //render view
        $this->load->view("template/skeleton",$toTpl);

    }
    
    
    /******************************
    * 
    *      ESPLOSO NEWS & PRESS
    * 
    ******************************/
    
    protected function _render_news($post = '') {
        
        $last = $this->uri->total_segments();
        $page_url = $this->uri->segment($last);
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        $this->template = "main";

        //meta
        $toTpl["author"] = $this->author;

        $this->session->set_userdata('currentPage', current_url());

        $toTpl["pagecontent"] = $this->post_model->get_post_by_url($page_url);

        /* Imposto il template della pagina */
        $template = 'template-dettaglio-news';
        
        $toBody['loading_quotes'] = $this->post_model->get_random_quote('loading-quotes');
        //LOCATIONS
        /* Ritorna i paesi in cui ci sono locations*/
        $all_countries = $this->locations_model->get_all_countries();
        $toHeader["countries"] = $all_countries;
        
        /* Meta */
        $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
        $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
        /* Carico la galleria associata alla pagina */            
        //$toTpl["pagecontent"]['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);

        /* Titolo barra del browser */
        $toTpl["title"] = $toTpl["pagecontent"]['title']." - ".$this->title;
        
        /* Breadcrumb nella pagina */
        //$toTpl["breadcrumb_title"] = $toTpl["pagecontent"]['title'];
        
        //data
        $toBody["content_body"] = $this->load->view($template,array_merge($this->data,$toTpl),true);


        $toHeader["nav"] = $this->load->view("template/nav",'',true);
        //$toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",'',true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


        //render view
        $this->load->view("template/skeleton",$toTpl);

    }
    
    
    public function _render_pagina_testuale() {
        
        $last = $this->uri->total_segments();
        $page_name = $this->uri->segment($last);
        
        //static
        $toTpl["javascript"] = $this->javascript;
        $toTpl["css"] = $this->css;
        $toTpl["fonts"] = $this->fonts;

        $this->template = "main";

        //meta
        $toTpl["author"] = $this->author;

        $this->session->set_userdata('currentPage', current_url());

        $toTpl["pagecontent"] = $this->page_model->get_page_full_content($page_name);

        /* Imposto il template della pagina */
        $template = $toTpl["pagecontent"]['template_name'];
        
        /* Loading quotes */
        $toBody['loading_quotes'] = $this->post_model->get_random_quote('loading-quotes');
        
        /* Meta */
        $toTpl["keywords"] = $toTpl["pagecontent"]['meta_keywords'];
        $toTpl["description"] = $toTpl["pagecontent"]['meta_description'];
        
        /* Carico la galleria associata alla pagina */            
        $toTpl['gallery'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['photogallery_id']);

        //$toTpl['big_slider'] = $this->gallery_model->get_photos_by_gallery_id($toTpl["pagecontent"]['extra_photogallery_id']);

        /* Titolo barra del browser */
        $toTpl["title"] = $toTpl["pagecontent"]['page_title']." - ".$this->title;
        
        //LOCATIONS
//        $all_countries = $this->locations_model->get_all_countries();
//        $toHeader["countries"] = $all_countries;
//        
//        $locations = array();
//        foreach($all_countries as $country) {
//            
//            array_push($locations, $this->locations_model->get_restaurants_for_country($country['country_code']));
//        }
//        
//        $toTpl["locations"] = $locations;
        
        /* Tipologia post associati a questa pagina */
        //$toTpl['posts'] = $this->post_model->get_posts_for_category_id($toTpl["pagecontent"]['post_type']);

        /* Breadcrumb nella pagina */
        //$toTpl["breadcrumb_title"] = $toTpl["pagecontent"]['title'];
        
        /* SOTTO-MENU PAGINA */
        //$toTpl['children'] = $this->getPageChildren($page_name, $toTpl);
        
        

        //data
        $toBody["content_body"] = $this->load->view($template,array_merge($this->data,$toTpl),true);


        $toHeader["nav"] = $this->load->view("template/nav",'',true);
        //$toHeader["basejs"] = $this->load->view("template/basejs",$this->data,true);

        $toBody["header"] = $this->load->view("template/header",$toHeader,true);
        $toBody["footer"] = $this->load->view("template/footer",'',true);

        $toTpl["body"] = $this->load->view("template/".$this->template,$toBody,true);


        //render view
        $this->load->view("template/skeleton",$toTpl);
    }

        
    public function getPageChildren($page_name, $toTpl) {
        $submenu = array();

        /* RECUPERO TUTTI I FIGLI DELLA PAGINA */
        $page_id = $this->page_model->get_id_by_name($page_name);
        $children_array = $this->page_model->get_page_children_of($page_id);
        $children = array();

        /* Verifico se la pagina principale va inserita nel submenu */
        if($toTpl["pagecontent"]['on_menu']) {
            array_push($submenu, $toTpl["pagecontent"]);
        }

        /* Verifico tutti i figli della pagina */
        foreach($children_array as $child) {
            $childcontent = $this->page_model->get_page_full_content($child['name']);
            if(isset($childcontent['photogallery_id'])) {
                $childcontent['gallery'] = $this->gallery_model->get_photos_by_gallery_id($childcontent['photogallery_id']);
            }
            array_push($children, $childcontent);

            /* Verifico se la pagina figlia va inserita nel menu */
            if($child['on_menu'] == 1) {
                array_push($submenu, $childcontent);
            }
        }
        return array('children' => $children, 'submenu' => $submenu);
    }

}
