<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pages_model extends grocery_CRUD_model {


    var $table = 'pages';  // Tabella madre
//    var $table_categories = 'post_categories'; // Tabella categoria
//    var $post_photogalleries_table = 'post_photogalleries'; // Tabella photogallery
//    var $photogalleries_table = 'photogalleries_photos'; // Tabella foto delle photogallery
//    var $photos_table = 'photos'; // tabella foto
//    var $photos_i18n_table = 'photos_i18n'; // tabella decodifica lingua foto
//    var $post_files_table = 'post_files'; // tabella dei file di un post
    var $admins_table = 'admins'; // tabella degli admin
    
    
    /**
     * Author Maicol Cantagallo
     * Responsability model list of news
     * @return type 
     */
    public function get_list(){
        if($this->table === null)
            return false;

        
        $select = "{$this->table}.*";
//        $select .= ", CONCAT(".$this->admins_table.".first_name, ' ',".$this->admins_table.".last_name) as creatore";

        if(!empty($this->relation))
            foreach($this->relation as $relation) {
                list($field_name , $related_table , $related_field_title) = $relation;
                $unique_join_name = $this->_unique_join_name($field_name);
                $unique_field_name = $this->_unique_field_name($field_name);

                if(strstr($related_field_title,'{'))
                    $select .= ", CONCAT('".str_replace(array('{','}'),array("',COALESCE({$unique_join_name}.",", ''),'"),str_replace("'","\\'",$related_field_title))."') as $unique_field_name";
                else	  
                    $select .= ", $unique_join_name.$related_field_title as $unique_field_name";

                if($this->field_exists($related_field_title))
                    $select .= ", {$this->table}.$related_field_title as '{$this->table}.$related_field_title'";
            }
            
           
            $this->db->select($select, false);
            
           // $this->db->join($this->admins_table, $this->admins_table.".id = ".$this->table.".created_by");

            $results = $this->db->get($this->table)->result();

            return $results;
    }
}

?>