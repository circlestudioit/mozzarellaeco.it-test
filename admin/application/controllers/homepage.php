<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage extends BP_Controller {
    
    public function __construct(){  
        parent::__construct();
    }

    public function index()
    {
        /*Page dedicated JS - CSS - Google Fonts
        $this->javascript = array("homepage.js");
        $this->css = array("BP/homepage.css");
        */
        
        $this->title = "Pannello di Amministrazione";
        
        $this->css = array("admin.css");
        
        if (!$this->login->isLogged()) {
            redirect('/admin_login');
        }
        else {
            $this->lang->load('main', $this->session->userdata('lang'));
            $this->session->set_userdata('currentPage', $this->uri->uri_string());

            
            $this->GFont = array("Lato:300,400,700");

            /*Define single page content as usual*/
//            $toView['page_content'] = date("H:i:s");
//            $toView['other_data'] = "<p>See you!</p>";

            /*short cut to load->view("pages/page_name",$content,true)*/
            $this->build_content();

            $this->render_page();
        }
    }
}

/*End of file homepage.php*/
/*Location .application/controllers/example.php*/