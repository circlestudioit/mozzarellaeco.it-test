<?php

class Pages extends BP_Controller {  // modify Controller Name


    function __construct() {
        parent::__construct();

        $this->load->library('grocery_CRUD'); // istanza crud
        $this->active_langs         = $this->config->item('active_langs');
        $this->module               = 'pages';
        $this->table                = 'pages';
        $this->table_i18n           = 'pages_i18n';
        $this->subject              = 'Pagine';
    }


    /**
     * Author: Raffaele Rotondo
     * Responsability: attraverso il CRUD vengono stabiliti i dati da 
     * visualizzare in fase di view, edit ed insert
     * @param type $output 
     */
    function index() {
        $this->css = array("admin.css");
        
        try{
            $crud = new grocery_CRUD();

            $crud->set_theme('flexigrid');    
            //$crud->set_model('pages_model');
            $crud->set_table($this->table);                                       
            $crud->set_subject($this->subject);   
            $crud->unset_print();
            $crud->unset_export();
            $crud->where($this->table.'.name !=', 'ROOT');
            $crud->set_relation('template_id', 'pages_templates', 'template_name');
            $crud->set_relation('parent_id', 'pages', 'name');
            $crud->set_relation('photogallery_id', 'photogalleries', 'name');
            $crud->set_relation('post_type', 'post_categories', 'name');
            $crud->set_relation('menu_id','menu','name',array('is_item' => FALSE));
            
            $crud->set_field_upload('thumb', $this->config->item('pages_thumb'));                
            $crud->set_field_upload('image', $this->config->item('pages_image'));
            $crud->set_field_upload('cover', $this->config->item('pages_cover'));
			
            $crud->unset_texteditor('meta_description');
            
            $crud->display_as('template_id','Template'); 
            $crud->display_as('parent_id','Genitore');
            $crud->display_as('name','Nome');
            $crud->display_as('published','Pubblicata');
            $crud->display_as('thumb','Miniatura');
            $crud->display_as('image','Immagine');
            $crud->display_as('cover','Copertina');
            $crud->display_as('ord','Ordine');
            $crud->display_as('menu_id','Menu della pagina');
            $crud->display_as('photogallery_id','Galleria fotografica');
            $crud->display_as('post_type','Categoria post associati alla pagina');
            
            $crud->display_as('meta_keywords','META Keywords (SEO)');
            $crud->display_as('meta_description','META Description (SEO)');
            
//             For each anctive language set a flag insert post
            $array_language = $this->active_langs;
            for($i=0; $i<count($array_language); $i++){
                $crud->add_action('Language'.$array_language[$i], base_url(IMAGES.$array_language[$i].'_flag.png'), base_url($this->module.'/language/'.$array_language[$i].'/edit').'/');
            }
            
            $crud->fields('template_id', 'parent_id', 'name', 'published', 'menu_id', 'thumb', 'image', 'cover', 'ord', 'post_type', 'photogallery_id', 'meta_keywords', 'meta_description');   
            
            $crud->columns('template_id', 'parent_id', 'name', 'published', 'thumb');  
            
//            $crud->callback_after_update(array($this, 'update_user_edit'));
//            $crud->callback_after_insert(array($this, 'update_user_insert'));

            $output = $crud->render();
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/'.$this->module, $data , true);
            
            /* Tramite il render del template caricherò*/
            $this->render_crud_page();
            
            
        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }
   
    
    /**
     * Author: Raffaele Rotondo
     * Responsability: gestisce la modifica della lingua di una pagina
     */
    
    function language($_lang) {
        $this->css = array("admin.css");
        
        $last = $this->uri->total_segments();
        $record_num = $this->uri->segment($last);
        
        try {
            $crud = new grocery_CRUD();
            
            $crud->set_theme('flexigrid');
            $crud->set_table($this->table_i18n);
            $crud->set_subject('lingua '.$_lang);
            $crud->unset_back_to_list();
            $crud->unset_print();
            $crud->unset_export();
            
            /* Imposto la primary key con id e lingua, per poter editare la lingua giusta */
            $crud->set_primary_key(array('id' => $record_num,'lang' => $_lang));
            
            /* Sostituisco la update originale con quella personalizzata per l'inserimento di una nuova riga */
            $crud->callback_update(array($this,'update_this_language'));

            $crud->display_as('title','Titolo pagina (h1)');
            $crud->display_as('page_title','Titolo (barra del browser)');  
            $crud->display_as('headline','Sottotitolo (h2)');
            $crud->display_as('content','Contenuto');
            $crud->display_as('content_2','Note');
            
            $crud->edit_fields('title', 'page_title', 'headline', 'content', 'content_2');
            
            $output = $crud->render();
            
            /* Mando i file js e css di CRUD al template */
            $this->js_files = $output->js_files;
            $this->css_files = $output->css_files;
            
            /* Estraggo l'output della tabella e lo sparo nella view, che viene caricata come data del template */
            $data['output'] = $output->output;
            $this->output = $this->load->view('pages/'.$this->module, $data , true);
            
            /* Tramite il render del template caricherò*/
            $this->render_crud_page();
            

//            $this->_list_output($output);

        }catch(Exception $e){
                show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }
    
    function update_this_language($post_array, $primary_key) {
        
        /* Recupero la lingua dall'indirizzo */
        $last = $this->uri->total_segments();
        $lang = $this->uri->segment($last-2);
        
        try {
            /* Controllo se la lingua è già esistente nel db */
            $query = $this->db->get_where($this->table_i18n, array('id' => $primary_key, 'lang' => $lang));
            
            /* Se c'è già devo solo aggiornarla */
            if($query->num_rows() > 0) {
                $this->db->where('lang', $lang);
                $this->db->update($this->table_i18n, $post_array, array('id' => $primary_key));
                return true;
            }
            /* Altrimenti devo inserire una nuova riga con quella lingua, e poi aggiornarla con i dati del form */
            else {
                $data = array('id' => $primary_key, 'lang' => $lang);
                $this->db->insert($this->table_i18n, $data);
                $this->db->where('lang', $lang);
                $this->db->update($this->table_i18n, $post_array, array('id' => $primary_key));
            }
            
        } catch (Exception $ex) {
            return $ex;
        }

    }
    
}

?>