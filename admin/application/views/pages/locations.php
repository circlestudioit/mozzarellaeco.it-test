<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADPYYIs9JHLidT8BJ2G2o0GHBp5orlSYU&v=3.exp&libraries=places"></script>

<script>
function initialize() {
  console.log($("#field-lat").val());
  if($("#field-lat").length > 0 && $("#field-lat").val() !== '') {
      var mapOptions = {
        center: new google.maps.LatLng($("#field-lat").val(), $("#field-long").val()),
        zoom: 18
      };
  } else {
      var mapOptions = {
        center: new google.maps.LatLng(42.47201, 14.21370),
        zoom: 3
      };
  }
    
  
  map = new google.maps.Map(document.getElementById('map-canvas'),
    mapOptions);

  var input = /** @type {HTMLInputElement} */(
      document.getElementById('pac-input'));

  var types = document.getElementById('type-selector');
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);

  var infowindow = new google.maps.InfoWindow();
  marker = new google.maps.Marker({
    map: map,
    anchorPoint: new google.maps.Point(0, -29),
    draggable: true
  });
  
  if($("#field-lat").val() !== '') {
    marker.setPosition(new google.maps.LatLng($("#field-lat").val(), $("#field-long").val()));
    marker.setVisible(true);
  }

  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    place = autocomplete.getPlace();
    if (!place.geometry) {
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  // Why 17? Because it looks good.
    }
    marker.setIcon(/** @type {google.maps.Icon} */({
      url: place.icon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    }));
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    infowindow.open(map, marker);
    console.log(place.geometry);
    
    $("#field-lat").val(place.geometry.location.A.toFixed(5)); //gets the latitude
    $("#field-long").val(place.geometry.location.F.toFixed(5)); //gets the longitude
  });
  
    google.maps.event.addListener(marker, "dragend", function() {
        var point = marker.getPosition();
        console.log(point);
        map.panTo(point);
        $("#field-lat").val(point.A.toFixed(5)); //gets the latitude
        $("#field-long").val(point.F.toFixed(5)); //gets the longitude
    });

  // Sets a listener on a radio button to change the filter type on Places
  // Autocomplete.
  function setupClickListener(id, types) {
    var radioButton = document.getElementById(id);
    google.maps.event.addDomListener(radioButton, 'click', function() {
      autocomplete.setTypes(types);
    });
  }
 
  setupClickListener('changetype-all', []);
  setupClickListener('changetype-address', ['address']);
  setupClickListener('changetype-establishment', ['establishment']);
  setupClickListener('changetype-geocode', ['geocode']);
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
    
    
<div class="container">
    <div class="content">
        <h1>
            Gestione locations
        </h1>
        <br />
    <input id="pac-input" class="controls" type="text" placeholder="Enter a location">
    <div id="type-selector" class="controls">
      <input type="radio" name="type" id="changetype-all" checked="checked">
      <label for="changetype-all">All</label>

      <input type="radio" name="type" id="changetype-establishment">
      <label for="changetype-establishment">Establishments</label>

      <input type="radio" name="type" id="changetype-address">
      <label for="changetype-address">Addresses</label>

      <input type="radio" name="type" id="changetype-geocode">
      <label for="changetype-geocode">Geocodes</label>
    </div>
    <div id="map-canvas"></div>
    
    
<?php echo $output; ?>
    </div>
</div>