<div class="container">
    <div class="content">
        <h1>
            Gestione template delle gallerie di immagini
        </h1>
        <hr />
        <p>
            In questa sezione puoi gestire i template delle gallerie di immagini. Creare un template significa impostare altezza e 
            larghezza delle fotografie a seconda delle tue esigenze. Potrai poi assegnare un template ad ogni galleria di immagini 
            che utilizzerai. Le immagini della galleria verranno ridimensionate in base al template scelto.
        </p>
        <br />
<?php echo $output; ?>
    </div>
</div>