<img src="<?=base_url(IMAGES.'customer_logo.png')?>" class="login-logo" />
<div class="signup_wrap">

    <h4>Inserisci nome utente e password</h4>
    <div class="signin_form">
        <?php echo form_open("admin_login/login"); ?>
        <p>
            <label for="login_username">Nome utente:</label>
            <input type="text" id="login_username" name="login_username" value="<?php echo set_value('login_username'); ?>" />
        </p>
        <p>
            <label for="login_password">Password:</label>
            <input type="password" id="login_password" name="login_password" value="<?php echo set_value('login_password'); ?>" />
        </p>
        <p>
            <input type="submit" class="button green-submit" value="Accedi" />
        </p>
             <?php echo form_close(); ?>
    </div><!--<div class="signin_form">-->
    <a href="<?=base_url()?>registrati/recupera_password"><small>
        Hai dimenticato la password?
    </small></a>
    
    <div class="register-error">
        <?php //echo $error != '' ? "<p style='color: red; margin: 0'>".$error."</p>" : '' ?>
        <?php echo validation_errors('<div class="error">', '</div>'); ?>
    </div>
</div>

<div class="admin-home-footer">
    powered by <br />
    <a href="http://www.circlestudio.it" target="_blank">
    <img src="<?=base_url(IMAGES."circle-studio-logo.png")?>" /></a>
</div>