$('#generic-full-page').fullpage({
    //anchors: ['intro', 'tenute', 'chieti', 'pescara', 'teramo', 'aquila'],
    //sectionsColor: ['', '#FFF', '#666', '#999'],
    autoScrolling: false,
    css3: true,
    verticalCentered: true,
    slidesNavigation: false,
    loopHorizontal: false,
    slideSelector: '.tenuta-slide',
    controlArrows: false,
    resize : false
});

$(window).load(function() {
    $('ul.slides').height($('.content-interne-azienda').outerHeight());
     
    $('.flexslider').flexslider({
        animation: "fade",
        randomize: true,
        controlNav: false,
        directionNav: false
    });
});
  
$(window).resize(function() {
      $('ul.slides').height($('.content-interne-azienda').outerHeight());
});
