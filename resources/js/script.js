Environment = {
    //mobile or desktop compatible event name, to be used with '.on' function
    TOUCH_DOWN_EVENT_NAME: 'mousedown touchstart',
    TOUCH_UP_EVENT_NAME: 'mouseup touchend',
    TOUCH_MOVE_EVENT_NAME: 'mousemove touchmove',
    TOUCH_DOUBLE_TAB_EVENT_NAME: 'dblclick dbltap',

    isAndroid: function() {
        return navigator.userAgent.match(/Android/i);
    },
    isAndroidTablet: function() {
        return navigator.userAgent.match(/Android 3.0/i);
    },
    isBlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    isIOSTablet: function() {
        return navigator.userAgent.match(/iPad/i);
    },
    isIOSPhone: function() {
        return navigator.userAgent.match(/iPhone|iPod/i);
    },       
    isOpera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    isWindows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    isChromeMobile: function() {
        return navigator.userAgent.match(/Chrome/[.0-9]* /Mobile/i);
    },
//    isChromeTablet: function() {
//       return navigator.userAgent.match('Chrome/[.0-9]* (?!Mobile)'); 
//    },
    
    isMobile: function() {
        return (Environment.isAndroid() || Environment.isBlackBerry() || Environment.isIOSPhone() || 
                Environment.isOpera() || Environment.isWindows() || Environment.isChromeMobile());
    },
    isTablet: function() {
        return (Environment.isIOSTablet() || Environment.isAndroidTablet());
    }
};

$(document).ready(function() {
    
    if(Environment.isMobile()) { //MOBILE
        //<![CDATA[
        $(window).load(function() { // makes sure the whole site is loaded
            setTimeout(function(){
                $("#status").fadeOut(); // will first fade out the loading animation
                $("#preloader").delay(150).fadeOut("slow", function() {                 
                    
                    
                }); // will fade out the white DIV that covers the website.
            }, 500);
        })
        //]]>
    }
    
    else if(Environment.isTablet()) { //TABLET
        //<![CDATA[
        $(window).load(function() { // makes sure the whole site is loaded
            setTimeout(function(){
                $("#status").fadeOut(); // will first fade out the loading animation
                $("#preloader").delay(150).fadeOut("slow", function() {
                    
                    
                    
                }); // will fade out the white DIV that covers the website.
            }, 500);
        })
        //]]>
    }
    
    else { //DESKTOP
        //<![CDATA[
        $(window).load(function() { // makes sure the whole site is loaded
            setTimeout(function(){
                $("#status").fadeOut(); // will first fade out the loading animation
                $("#preloader").delay(150).fadeOut("slow", function() {
                    

                }); // will fade out the white DIV that covers the website.
            }, 500);
        });
        //]]>
    }
    
    // Funzioni chiamate su tutti i dispositivi
    if(document.getElementById('video')) {
        resizeVideo();
        //setVideoSize();
        playVideo();

    }  
    
    /* fix - apertura menu toggle su mobile */
    $('[data-toggle=dropdown]').each(function() {
        this.addEventListener('click', function() {}, false);
    });
    
    $('button.navbar-toggle').click(function() {
        $('body').css('overflow', 'hidden');
    });
    
    setTimeout(function() { 
        $(".h0").css("margin-top","0px");
        $(".h0").css("opacity","1"); 
    });
                 
    
    $("a.ajax-link").click(function(e) {
        //prevent default action
        e.preventDefault();

        //define the target and get content then load it to container
        var l = $(this).attr("href");
        link = l.split("/");
        baselink = link[0];
        pagelink = link[1];
        //console.log(baselink+'/'+pagelink);
        openSubPage(baselink, pagelink);
    });

    
    $('#menu-icon').click(function() {
        if($(this).hasClass('on')){
            $('#menu-icon').removeClass('on');
            $('.nav').fadeOut();
            $('body, html').css('overflow','visible');
        }
        else{
            $('.nav').fadeIn();
         $('#menu-icon').addClass('on');
         $('body, html').css('overflow','hidden');
        }
    });
    
    $('#format, #bufale, #tagliere, #last').height($(window).height());

    /* Slider Home Page */
    if(document.getElementById('sliderhome')) {
        $('#sliderhome-slides-ul').height($(window).height());
        
        $('.flexslider').flexslider({
            animation: "fade",
            randomize: false,
            controlNav: true,
            directionNav: false,
            slideshow: true
        });   
    }
    
    /* Slider Pagina Store */
    if(document.getElementById('slider-store')) {
        //$('#sliderpiatti-slides-ul').height($(window).height());
        
        $('.flexslider-piatti').flexslider({
            animation: "slide",
            randomize: false,
            controlNav: true,
            directionNav: false,
            slideshow: true
        });   
    }

    
    $('a[href^="#"]').click(function(e) {
    // Prevent the jump and the #hash from appearing on the address bar
    e.preventDefault();
    // Scroll the window, stop any previous animation, stop on user manual scroll
    // Check https://github.com/flesler/jquery.scrollTo for more customizability
    $(window).stop(true).scrollTo(this.hash, {duration:1000, interrupt:true});
});
    
    
}); //document.ready
 
$(window).resize(function() {

    $('#format, #bufale, #tagliere, #last').height($(window).height());
    
    resizeVideo();

});

//$(window).on("scroll, touchmove", function() {
$(window).bind("scroll", function() {
//   setTimeout(function() {
//        $('.back-to-top').stop().fadeOut();
//    }, 2000); 
    
    /* PFEIL */
    if ($(window).scrollTop() <= 0)
    {
        $('.down-arrow').fadeIn();
        
    }
    else
    {
        $('.down-arrow').fadeOut();
    }
    /* /PFEIL */

    /* START PARALLAX */
//    if ($(window).scrollTop() < window.innerHeight)
//    {
//            var precent = Math.min(1, $(window).scrollTop() / window.innerHeight);
//            //$("#content #header #header_parallax").css("top", (precent * -5)+"%");
//            //$("#content #header #header_parallax #header_text").css("opacity", Math.max(1-precent*2, 0));
////            if(Math.max(precent*2, 0)< 0.8)
////                $('.opacity-layer').css("opacity", Math.max(precent*2, 0));
//
//            //$('.menubar').fadeIn();
//    }
    /* /START PARALLAX */
});