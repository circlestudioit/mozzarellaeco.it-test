$(window).load(function () {
    $('.page').css('width', $(window).width()/3+'px');
    $('.ingredienti img').css('width', $(window).width()/3+'px');
    
    var bott_w = $('.bottiglie').children().length;
    console.log(bott_w);
    
    bott_w = bott_w * $('.page').width()*1.1;
    //console.log(bott_w);
    $('.bottiglie').css('width', bott_w+'px');
    
    
    var ingredienti_w = $('.ingredienti').children().length;
    ingredienti_w = ingredienti_w * $('.ingredienti img').width();
    //console.log(ingredienti_w);
    $('.ingredienti').css('width', ingredienti_w+'px');
    
     $.jInvertScroll(['.scroll']);
     
//    var h = $(window).height();
//    $('li.page').css('line-height', h+'px');
    
    $('#footer').transit({ 'bottom' : '0em'}, 250, 'ease');
    $('#header').transit({ 'top'  : '0em'}, 700, 'ease');
    
});

function showNotizia(newsId) {
    var h = $("li[data-id="+newsId+"]").height();
    $("li[data-id="+newsId+"] .sottotitolo-news").css({'height' : h+'px', 'margin-top' : '-'+h+'px'});
    $("li[data-id="+newsId+"]").css({'background-color' : '#000', 'opacity' : '0.8'});
    $("li[data-id="+newsId+"] h1").css({'opacity' : '0.1'});
    $("li[data-id="+newsId+"] .sottotitolo-news").fadeIn();
}

function hideNotizia(newsId) {
    $("li[data-id="+newsId+"]").removeAttr('style');
    $("li[data-id="+newsId+"] h1").removeAttr('style');
    $("li[data-id="+newsId+"] .sottotitolo-news").fadeOut();
}


function blurify(id) {
    $('div#'+id+'.product-name-wrapper').fadeIn(700);
    
    $({blurRadius: 0}).animate({blurRadius: 4}, {
        duration: 500,
        easing: 'linear', // or "linear"
                          // use jQuery UI or Easing plugin for more options
        step: function() {
//            console.log(this.blurRadius);
            $('#'+id).css({
                "filter": "blur("+this.blurRadius+"px)", 
                "-webkit-filter": "blur("+this.blurRadius+"px)", 
                "-moz-filter": "blur("+this.blurRadius+"px)",
                "-o-filter": "blur("+this.blurRadius+"px)", 
                "-ms-filter": "blur("+this.blurRadius+"px)",
                "filter": "url(../css/blur.svg#blur)",
                "filter":"progid:DXImageTransform.Microsoft.Blur(PixelRadius='"+this.blurRadius+"')"
            });
        }
    });
}

function deblurify(id) {
    $('div#'+id+'.product-name-wrapper').fadeOut(300);
    $({blurRadius: 4}).animate({blurRadius: 0}, {
        duration: 500,
        easing: 'linear', // or "linear"
                         // use jQuery UI or Easing plugin for more options
        step: function() {
//            console.log(this.blurRadius);
            $('#'+id).css({
                "filter": "blur("+this.blurRadius+"px)", 
                "-webkit-filter": "blur("+this.blurRadius+"px)", 
                "-moz-filter": "blur("+this.blurRadius+"px)",
                "-o-filter": "blur("+this.blurRadius+"px)", 
                "-ms-filter": "blur("+this.blurRadius+"px)",
//                "filter": "url(../css/blur.svg#blur)",
                "filter":"progid:DXImageTransform.Microsoft.Blur(PixelRadius='"+this.blurRadius+"')"
            });
        }
    });
}

function showScheda(id) {
    $('#scheda-'+id).fadeIn();
}

function closeScheda(id) {
    $('#scheda-'+id).fadeOut();
}