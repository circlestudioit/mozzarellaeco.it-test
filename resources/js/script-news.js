$(document).ready(function() {
    initNewsFiltering();
});

$(window).bind("scroll", function() {
    /* PFEIL */
    if ($(window).scrollTop() <= 0)
    {
        $('#news-highlight').fadeIn(500);
        $('.maintitle-focuson').css({'opacity' : 1});
        $('.focuson-line').css({'opacity' : 1});
    }
    else
    {
        $('#news-highlight').fadeOut(500);
        $('.maintitle-focuson').css({'opacity' : 0});
        $('.focuson-line').css({'opacity' : 0});
    }
    
});