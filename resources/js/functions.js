function playVideo() {
    $('#video').fadeIn();

    iframe = document.querySelector('iframe#vimeo');
    player = new Vimeo.Player(iframe);
    player.setVolume(1);

    player.play();
    
    $(".sidedock,.controls").remove();
    
    player.on('ended', function() {
        $('#video').fadeOut(function() {
            $(".poster").fadeIn(500);
            $(".benvenuto").fadeIn(1000);
            $(".down-arrow").fadeIn(1500);
        });
        
        
    });
    
}

function resizeVideo() {
    var w = $(window).width();
    var h = w*1080/1920;
    $('#video').css({'width' : w, 'height' : h});
    $('#video video').attr({'width' : w, 'height' : h});
    
    $('#video iframe').attr("width", w);
    $('#video iframe').attr("height", h);
    
    $('#videoslide iframe').attr("width", w);
    $('#videoslide iframe').attr("height", h);
}

function setVideoSize() {
    
    var w = $(window).width();
    var h = w*1080/1920;
    $('#video').css({'width' : w, 'height' : h});
    //, 'margin-top' : $('.navbar').innerHeight()+'px'
    $('#video video').attr({'width' : w, 'height' : h});
    
    $('#videoslide iframe').attr("width", w);
    $('#videoslide iframe').attr("height", h);

}

function initSquare(className) {
    
    w_square = $('.w_square').width();
	$('.w_square').css('height', w_square+'px');
    $('.'+ className +'').css('height', w_square+'px');
    

}

function openLangMenu()
{
   $('.list-lang').fadeIn().slideDown();
}

function openLocations()
{
    $('.nav-locations').slideDown();
}

function openCities()
{
    $('#nav-cities').slideDown();
    
}

//function openDrop(location) {
//    $('#'+location).show();
//}

function openRestaurants(country)
{
    $('li#'+country).addClass('active').siblings().removeClass('active');
    $.ajax({
        type: 'POST',
        url: document.location.origin+'/home/get_restaurants_for_country/'+country, /* pageurl */
        success: function(data) {
            /* Carico il contenuto del lavoro nel div */
            var pdata = $.parseJSON(data);
            //console.log(pdata);
            $('#nav-restaurants').empty();
            for (i = 0; i < pdata.length; i++) { 
                //console.log(pdata[i]);
                if(pdata[i].published == 1 && pdata[i].next_opening == 0) {
                    console.log(pdata[i].loc_name);
                $('#nav-restaurants').append(
                    $('<li><a href="'+document.location.origin+'/location/'+pdata[i].loc_name+'">'+pdata[i].store_short_name+'<br /><small class="city-name-nav">'+pdata[i].city+'</small></a></li>'));
                }
                else if(pdata[i].next_opening == 1) {
                    $('#nav-restaurants').append(
                    $('<li class="inactive"><a href="'+document.location.origin+'/location/'+pdata[i].loc_name+'">'+pdata[i].store_short_name+'<br /><small class="city-name-nav inactive">'+pdata[i].city+'</small></a></li>'));
                }

            }
            $('#nav-restaurants').slideDown();
        },
        error: function(data) {
            console.log('ajax error: '+data);
        }
    });
    
    //to change the browser URL to the given link location
//    if(pagelink!=window.location) {
//        window.history.pushState({path:'/'+pagelink},'', '/'+pagelink);
//    }
    //stop refreshing to the page given in
    return false;
    
}

function send_mail() {
    
      $.ajax({
        type: 'POST',
        url: document.location.origin+'/home/send_mail', /* pageurl */
        success: function(data) {
            
            /* Carico il contenuto del lavoro nel div */
            //var pdata = $.parseJSON(data);
            $('#form-success').html("Grazie per aver inviato la tua richiesta.");
            
        },
        error: function(data) {
            console.log('ajax error: '+data);
        }
    });
}

function setupMap(height) {
    $('.map-canvas').height(height);
}

/* Menu Action */
function openMenu() {
    $("#menu").fadeIn();
    $("#menu-icon").addClass('on');
    
}

function closeMenu() {
    $("#menu").slideLeftHide();
}

function openProdotti() {
    $(".other_products_nav").slideLeftShow();
}

function closeProdotti() {
    $(".other_products_nav").slideLeftHide();
}


jQuery.fn.extend({
  slideRightShow: function() {
    return this.each(function() {
        $(this).show('slide', {direction: 'right'}, 500);
    });
  },
  slideLeftHide: function() {
    return this.each(function() {
      $(this).hide('slide', {direction: 'left', easing: 'easeOutExpo'}, 700);
    });
  },
  slideRightHide: function() {
    return this.each(function() {
      $(this).hide('slide', {direction: 'right'}, 500);
    });
  },
  slideLeftShow: function() {
    return this.each(function() {
      $(this).show('slide', {direction: 'left', easing: 'easeOutQuart'}, 1000);
    });
  }
});

